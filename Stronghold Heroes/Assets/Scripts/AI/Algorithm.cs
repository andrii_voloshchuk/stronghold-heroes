﻿using System.Collections.Generic;
using UnityEngine;

public class Algorithm
{
    private const string DATA_PATH = "Data/AI";

    private Dictionary<Rule, DECISION> rules;

    public Algorithm()
    {
        rules = new Dictionary<Rule, DECISION>();
        string data = ResourcesManager.Load<TextAsset>(DATA_PATH).text;
        string[] lines = data.Split('\n');
        foreach(string line in lines)
        {
            if(!string.IsNullOrEmpty(line))
            {
                string[] parameters = line.Split(',');
                DECISION decision = GameHelper.ToEnum<DECISION>(parameters[5]);
                           rules.Add(new Rule(parameters), decision);
            }
        }
    }

    public DECISION GetDecision(Rule rule)
    {
        if(rules.ContainsKey(rule))
        {
            return rules[rule];
        }
        else
        {
            Debug.LogWarning("Cannot find decision for rule " + rule.ToString());
        }
        return DECISION.MOVE_TO_WEAKEST;
    }	
}
