﻿public class Rule  
{
    public UNIT_CLASS unitClass;

    public HP_LEVEL hpLevel;

    public ALLIES_ALIVE alliesAlive;

    public ENEMIES_IN_RANGE enemiesInRange;

    public SPECIAL_POWER_AVAILABLE specialPowerAvailable;

    public Rule()
    {

    }

    public Rule(string[] parameters)
    {
        unitClass = GameHelper.ToEnum<UNIT_CLASS>(parameters[0]);
        hpLevel = GameHelper.ToEnum<HP_LEVEL>(parameters[1]);
        alliesAlive = GameHelper.ToEnum<ALLIES_ALIVE>(parameters[2]);
        enemiesInRange = GameHelper.ToEnum<ENEMIES_IN_RANGE>(parameters[3]);
        specialPowerAvailable = GameHelper.ToEnum<SPECIAL_POWER_AVAILABLE>(parameters[4]);
    }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return false;
        if (obj.GetType() != GetType())
            return false;
        Rule other = (Rule)obj;
        return unitClass == other.unitClass && hpLevel == other.hpLevel && alliesAlive == other.alliesAlive
            && enemiesInRange == other.enemiesInRange && specialPowerAvailable == other.specialPowerAvailable;
    }

    public override int GetHashCode()
    {
        return (int)unitClass * ((int)alliesAlive + (int)hpLevel) + (int)enemiesInRange * (int)specialPowerAvailable * 2;
    }

    public override string ToString()
    {
        return "{" + unitClass + "," + hpLevel + "," + alliesAlive + "," + enemiesInRange + "," + specialPowerAvailable + "}";
    }
}
