﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerProfile
{
    private const string SAVE_KEY = "PlayerProfile";
    public const string DEFAULT_NAME = "Lord Crusader";
    private const string GOLD_KEY = "Gold";
    private const string PROGRESS_KEY = "Progress";
    private const string DEFAULT_PROGRESS_PATH = "Data/Progress";

    [Serializable]
    public class Level
    {
        public int name;
        public int state;

        public Level(int name, int state)
        {
            this.name = name;
            this.state = state;
        }
    }

    [Serializable]
    public class Progress
    {
        public List<Level> levels;
    }

    private static PlayerProfile instance;

    private Progress progress;

    private Player player;

    private int gold;

    public static bool Exists { get { return PlayerPrefs.HasKey(SAVE_KEY); } }

    public static void CreateInstance()
    {
        instance = new PlayerProfile();
        if(PlayerPrefs.HasKey(GOLD_KEY))
        {
            instance.gold = PlayerPrefs.GetInt(GOLD_KEY);
        }
    }

    public static void Create(string name)
    {
        instance.player = new Player(name);
        instance.player.units = GameHelper.GetRandomUnits(6);
        instance.player.avatar = AVATAR.RICHARD;
        instance.gold = 1000;
        Save();
    }

    public static Player Load()
    {
        if(Exists)
        {
            string json = PlayerPrefs.GetString(SAVE_KEY);
            instance.player = new Player(JsonUtility.FromJson<Player>(json));
        }
        else
        {
            Create(DEFAULT_NAME);
        }
        return instance.player;
    }
	

    public static void Update(Player player)
    {
        instance.player = player;
    }

    public static void Save()
    {
        string json = JsonUtility.ToJson(instance.player);
        PlayerPrefs.SetString(SAVE_KEY, json);
        PlayerPrefs.SetInt(GOLD_KEY, instance.gold);
        SaveProgress();
        PlayerPrefs.Save();
    }

    public static void Delete()
    {
        PlayerPrefs.DeleteAll();
    }

    public static Player GetPlayer()
    {
        return instance.player;
    }

    public static int GetGold()
    {
#if INFINITE_GOLD
        return 1000000;
#else
        return instance.gold;
#endif
    }

    public static void AddGold(int amount)
    {
        long delta = instance.gold + amount;
        if (delta < 0)
            delta = 0;
        if (delta > int.MaxValue)
            delta = int.MaxValue;
        instance.gold = (int)delta;
    }

    public static void UpgradeUnit(Unit unit, int index)
    {
        instance.player.units[index] = new Unit(unit);
    }

    public static void AddUnit(Unit unit)
    {
        instance.player.units.Add(unit);
    }

    public static List<Level> GetProgress()
    {
#if UNLOCK_ALL
        List<Level> levels = new List<Level>();
        for(int i = 0; i < 30; i++)
        {
            levels.Add(new Level(i, 2));
        }
        levels[levels.Count - 1].state = 1;
        return levels;
#else
        return instance.progress.levels;
#endif

    }

    public static void UpdateLevel(int index)
    {

        Level level = instance.progress.levels[index];

        if (level.state < 2)
        {
            level.state = 2;
        }
        if (index + 1 < instance.progress.levels.Count)
        {
            if (instance.progress.levels[index + 1].state < 1)
            {
                instance.progress.levels[index + 1].state = 1;
            }
        }
    }

    public static void InitProgress()
    {
        instance.progress = new Progress();
        if (PlayerPrefs.HasKey(PROGRESS_KEY))
        {
            JsonUtility.FromJsonOverwrite(PlayerPrefs.GetString(PROGRESS_KEY), instance.progress);
        }
        else
        {
            TextAsset progress = ResourcesManager.Load<TextAsset>(DEFAULT_PROGRESS_PATH);
            JsonUtility.FromJsonOverwrite(progress.text, instance.progress);
            SaveProgress();
        }
    }

    public static void SaveProgress()
    {
        PlayerPrefs.SetString(PROGRESS_KEY, JsonUtility.ToJson(instance.progress));
        PlayerPrefs.Save();
    }
}


