﻿using UnityEngine;
using System.Collections.Generic;

public class MusicManager : MonoBehaviour
{
    private const string KEY = "music";

    public List<AudioClip> music;

    private static MusicManager instance = null;
    public static MusicManager Instance { get { return instance; } }

    private AudioSource audioSource;

    private bool isEnabled = false;
    public bool IsEnabled { get{ return isEnabled; } set { isEnabled = value; } }

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        else
            instance = this;
    }

    private void Start () 
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.loop = true;
        if(PlayerPrefs.HasKey(KEY))
        {
            SwitchMusic(PlayerPrefs.GetInt(KEY) == 1);
        }
        else
        {
            SwitchMusic(true);
        }
        PlayOneClip("MainMenu");
    }

    public void SwitchMusic(bool value)
    {
        isEnabled = value;
        audioSource.mute = !value;
        PlayerPrefs.SetInt (KEY, isEnabled ? 1 : 0);
        PlayerPrefs.Save ();
    }

    private AudioClip GetClipByName(string clipName)
    {
        foreach(AudioClip clip in music)
        {
            if (clip.name == clipName)
                return clip;
        }
        return music[0];
    }

    public void PlayRandomClip(string name, int min, int max)
    {
        int random = Random.Range (min, max + 1);
        string clip = name + "_" + random;
        PlayOneClip(clip);
    }

    public void PlayOneClip(string name)
    {
        if (audioSource.clip == null || (audioSource.clip != null && audioSource.clip.name != name))
        {
            audioSource.Stop();
            audioSource.clip = GetClipByName(name);
            audioSource.Play();
        }
    }

    public void Stop()
    {
        audioSource.Stop();
    }
}
