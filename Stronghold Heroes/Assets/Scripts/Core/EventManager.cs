﻿public class EventManager  
{
    private static EventManager instance = null;
    public static EventManager Instance
    {
        get { return instance; }
    }
 
    public static void CreateInstance()
    {
        instance = new EventManager();
    }

    public delegate void StartBattleActionEventHandler(int initiatorId, ACTION_TYPE type, BattleAction.SimpleAction action);
    public static event StartBattleActionEventHandler onStartBattleAction;
    public void OnStartBattleAction(int initiatorId, ACTION_TYPE type, BattleAction.SimpleAction action)
    {
        if (onStartBattleAction != null)
            onStartBattleAction(initiatorId, type, action);
    }

    public delegate void FinishBattleActionEventHandler(int initiatorId, ACTION_TYPE type);
    public static event FinishBattleActionEventHandler onFinishBattleAction;
    public void OnFinishBattleAction(int initiatorId, ACTION_TYPE type)
    {
        if (onFinishBattleAction != null)
            onFinishBattleAction(initiatorId, type);
    }

    public delegate void ProcessBattleActionEventHandler();
    public static event ProcessBattleActionEventHandler onProcessBattleAction;
    public void OnProcessBattleAction()
    {
        if (onProcessBattleAction != null)
            onProcessBattleAction();
    }

    public delegate void BackPressedEventHandler();
    public static event BackPressedEventHandler onBackPressed;
    public void OnBackPressed()
    {
        if (onBackPressed != null)
            onBackPressed();
    }

    public delegate void ShowInputWindowEventHandler();
    public static event ShowInputWindowEventHandler onShowInputWindow;
    public void OnShowInputWindow()
    {
        if(onShowInputWindow != null)
        {
            onShowInputWindow();
        }
    }

    public delegate void HideInputWindowEventHandler();
    public static event HideInputWindowEventHandler onHideInputWindow;
    public void OnHideInputWindow()
    {
        if (onHideInputWindow != null)
        {
            onHideInputWindow();
        }
    }

    public delegate void ShowDialogWindowEventHandler(SmallWindow.TITLE title, DialogWindow.OnOptionPressed yesPressed, DialogWindow.OnOptionPressed noPressed = null);
    public static event ShowDialogWindowEventHandler onShowDialogWindow;
    public void OnShowDialogWindow(SmallWindow.TITLE title, DialogWindow.OnOptionPressed yesPressed, DialogWindow.OnOptionPressed noPressed = null)
    {
        if (onShowDialogWindow != null)
        {
            onShowDialogWindow(title, yesPressed, noPressed);
        }
    }

    public delegate void HideDialogWindowEventHandler();
    public static event HideDialogWindowEventHandler onHideDialogWindow;
    public void OnHideDialogWindow()
    {
        if (onHideDialogWindow != null)
        {
            onHideDialogWindow();
        }
    }

    public delegate void OnSceneLoadEventHandler();
    public static event OnSceneLoadEventHandler onSceneLoad;
    public void OnSceneLoad()
    {
        if (onSceneLoad != null)
        {
            onSceneLoad();
        }
    }
}
