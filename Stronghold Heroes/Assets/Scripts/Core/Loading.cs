﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Loading : MonoBehaviour 
{
    public Transform progress;

    private AsyncOperation loading = null;

    public IEnumerator LoadScene(string sceneName)
    {
        loading = SceneManager.LoadSceneAsync(sceneName);
        yield return loading;
    }

    private void OnLoadingCompleted()
    {
        loading = null;
        progress.localScale = new Vector3(0f, 1f, 1f);
        Core.Instance.OnLoadingCompleted();
    }

    private void Update()
    {
        if (loading != null)
        {
            if (!loading.isDone)
            {
                progress.localScale = new Vector3(loading.progress, 1f, 1f);
            }
            else
            {
                progress.localScale = new Vector3(1f, 1f, 1f);
                OnLoadingCompleted();
            }
        }
    }
}
