﻿using UnityEngine;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour 
{
    private const string SOUNDS_PATH = "Sounds/";
    private const string KEY = "sound";

    public List<AudioClip> sounds;

    private static SoundManager instance = null;
    public static SoundManager Instance { get { return instance; } }

    private AudioSource audioSource;

    private bool isEnabled = false;
    public bool IsEnabled { get{ return isEnabled; } set { isEnabled = value; } }

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        else
            instance = this;
    }

	private void Start () 
    {
        audioSource = GetComponent<AudioSource>();
        if(PlayerPrefs.HasKey(KEY))
        {
            SwitchSound(PlayerPrefs.GetInt(KEY) == 1);
        }
        else
        {
            SwitchSound(true);
        }
	}

    public void SwitchSound(bool value)
    {
        isEnabled = value;
        PlayerPrefs.SetInt(KEY, isEnabled ? 1 : 0);
        PlayerPrefs.Save ();
    }
	
    private AudioClip GetClipByName(string clipName)
    {
        foreach(AudioClip clip in sounds)
        {
            if (clip.name == clipName)
                return clip;
        }
        return sounds[0];
    }

    public void PlaySoundFromResources(string path)
    {
        if (isEnabled)
        {
            AudioClip clip = ResourcesManager.Load<AudioClip>(SOUNDS_PATH + path);
            audioSource.PlayOneShot(clip);
        }
    }

    public void PlayRandomSoundFromResources(string path, int min, int max)
    {
        if (isEnabled)
        {
            int random = Random.Range(min, max + 1);
            PlaySoundFromResources(path + random);
        }
    }

    public void PlayRandomSound(string name, int min, int max)
    {
        if (isEnabled) 
        {
            int random = Random.Range (min, max + 1);
            audioSource.PlayOneShot (GetClipByName (name + "_" + random));
        }
    }

    public void PlayOneSound(string name)
    {
        if(isEnabled)
            audioSource.PlayOneShot(GetClipByName(name));
    }

    public void Play(string clipName)
    {
        if (isEnabled)
        {
            audioSource.clip = GetClipByName(clipName);
            audioSource.Play();
        }
    }

    public void Stop(string clipName)
    {
        if (audioSource.clip != null)
        {
            if (audioSource.clip.name == clipName)
                audioSource.clip = null;
        }
    }
}
