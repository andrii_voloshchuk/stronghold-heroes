﻿using UnityEngine;

public class Core : MonoBehaviour 
{
    private static Core instance = null;
    public static Core Instance
    {
        get { return instance; }
    }

    public Loading loading;

    public SpriteDictionary spriteDictionary;

    private Player player1;
    private Player player2;

    public GAME_MODE gameMode;

    public int level;

    private void Awake()
    {
        if (instance != null & instance != this)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(gameObject);
        Init();
    }

    private void Init()
    {
        GameHelper.LoadInitialData();
        EventManager.CreateInstance();
        spriteDictionary.Init();
        InitPlayerProfile();
        TextDictionary.CreateInstance();
    } 

    private void Start()
    {
        LoadScene("MainMenu");
    }

    private void InitPlayerProfile()
    {
        PlayerProfile.CreateInstance();
        PlayerProfile.InitProgress();
    }

    public void OnLoadingCompleted()
    {
        loading.gameObject.SetActive(false);
    }

    public void LoadScene(string sceneName)
    {
        EventManager.Instance.OnSceneLoad();
        loading.gameObject.SetActive(true);
        StartCoroutine(loading.LoadScene(sceneName));
    }

    public void StartBattle(GAME_MODE gameMode, Player player1, Player player2, int level = 0)
    {
        this.level = level;
        this.gameMode = gameMode;
        this.player1 = player1;
        this.player2 = player2;
        LoadScene("Battle");
    }

    public Player[] GetPlayers()
    {
        Player[] players = new Player[2];
        players[0] = player1;
        players[1] = player2;

        return players;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            EventManager.Instance.OnBackPressed();
        }
    }
}
