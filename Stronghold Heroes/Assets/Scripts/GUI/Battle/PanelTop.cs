﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class PanelTop : MonoBehaviour 
{
    [Serializable]
    public struct PlayerAvatar
    {
        public Image flag;
        public Image avatar;
        public Text playerName;

        public void SetAvatar(Player player)
        {
            playerName.text = player.name;
            avatar.sprite = SpriteDictionary.Instance.GetAvatarSprite(player.avatar);
            flag.sprite = SpriteDictionary.Instance.GetTeamSprite(GameHelper.ToEnum<UNIT_TEAM_SPRITE>(player.unitTeam + "_FLAG"));
        }
    }

    public PlayerAvatar[] playerAvatars;

    public void Init(Player[] players)
    {
        for (int i = 0; i < playerAvatars.Length; i++)
        {
            playerAvatars[i].SetAvatar(players[i]);
        }
    }
}
