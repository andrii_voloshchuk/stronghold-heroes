﻿using UnityEngine;
using UnityEngine.UI;

public class TurnImage : MonoBehaviour 
{
    public Image unitIcon;
    public Image colorFlag;

    public Text levelText;

    public void SetUnit(Sprite unitSprite, Sprite flagSprite, string level)
    {
        unitIcon.sprite = unitSprite;
        colorFlag.sprite = flagSprite;
        levelText.text = level;
    }
}
