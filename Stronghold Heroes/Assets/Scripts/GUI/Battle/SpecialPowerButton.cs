﻿using UnityEngine;
using UnityEngine.UI;

public class SpecialPowerButton : MonoBehaviour 
{
    public Text coolDown;
    public Text specialPowerName;

    private Button button;
    private RectTransform rectTransform;

	private void Awake()
	{
        button = GetComponent<Button>();
        rectTransform = GetComponent<RectTransform>();
    }

    public void UpdateSpecialPower(SPECIAL_POWER_TYPE type, int cooldown)
    {
        UpdateSprites(type);
        string powerName = TextDictionary.GetText(type.ToString());
        specialPowerName.text = powerName;
        if(cooldown > 0)
        {
            button.interactable = false;
            coolDown.text = cooldown.ToString();
        }
        else
        {
            button.interactable = true;
            coolDown.text = string.Empty;
        }
    }

    private void UpdateSprites(SPECIAL_POWER_TYPE type)
    {
        SpriteState st = new SpriteState();
        SPECIAL_POWER_SPRITE spsUp = GameHelper.ToEnum<SPECIAL_POWER_SPRITE>(type + "_UP");
        Sprite spriteUp = SpriteDictionary.Instance.GetSpecialPowerSprite(spsUp);
        st.highlightedSprite = spriteUp;
        button.image.sprite = spriteUp;

        SPECIAL_POWER_SPRITE spsDown = GameHelper.ToEnum<SPECIAL_POWER_SPRITE>(type + "_DOWN");
        st.pressedSprite = SpriteDictionary.Instance.GetSpecialPowerSprite(spsDown);

        SPECIAL_POWER_SPRITE spsDisabled = GameHelper.ToEnum<SPECIAL_POWER_SPRITE>(type + "_DISABLED");
        st.disabledSprite = SpriteDictionary.Instance.GetSpecialPowerSprite(spsDisabled);

        rectTransform.sizeDelta = new Vector2(spriteUp.textureRect.width, spriteUp.textureRect.height);
        button.spriteState = st;
    }

    public void Block(bool value)
    {
        button.interactable = !value;
    }
}
