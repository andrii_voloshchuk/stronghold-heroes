﻿using UnityEngine;
using System.Collections.Generic;

public class PanelTurn : MonoBehaviour 
{
    public TurnImage[] images;

    public void UpdateTurnList(List<UnitTurn> turnList)
    {
        for (int i = 0; i < images.Length; i++)
        {
            if(i < turnList.Count)
            {
                images[i].gameObject.SetActive(true);
                Sprite shield = SpriteDictionary.Instance.GetTeamSprite(GameHelper.ToEnum<UNIT_TEAM_SPRITE>(turnList[i].UnitTeam + "_SHIELD"));
                Sprite sprite = SpriteDictionary.Instance.GetUnitSprite(turnList[i].Unit.unitType);
                string level =  turnList[i].Unit.level.ToString();
                images[i].SetUnit(sprite, shield, level);
            }
            else
            {
                images[i].gameObject.SetActive(false);
            }
        }
    }
}
