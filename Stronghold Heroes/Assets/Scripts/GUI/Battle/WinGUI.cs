﻿using UnityEngine;
using UnityEngine.UI;

public class WinGUI : MonoBehaviour 
{
    public GameObject noDeathBonus;
    public GameObject forwardButton;

    public Text winText;
    public Text killedAmount;
    public Text survivedAmount;
    public Text totalAmount;
    public Text noDeathAmount;

    public void Init(Player player, Player enemy, bool win)
    {
        winText.text = "You " + (win ? "win" : "lose");

        int killed = 0;
        foreach(Unit unit in enemy.units)
        {
            if(unit.IsDead)
            {
                killed += unit.level * 100;
            }
        }

        int count = 0;
        int survived = 0;
        foreach (Unit unit in player.units)
        {
            if (!unit.IsDead)
            {
                count++;
                survived += unit.level * 100;
            }
        }

        int noDeath = count == player.units.Count ? survived / 2 : 0;

        int total = killed + survived + noDeath;

        noDeathBonus.SetActive(noDeath > 0);

        killedAmount.text = killed.ToString();
        survivedAmount.text = survived.ToString();

        if(noDeath > 0)
        {
            noDeathAmount.text = noDeath.ToString();
        }

        totalAmount.text = total.ToString();

        PlayerProfile.AddGold(total);
        PlayerProfile.Save();

        forwardButton.SetActive(true);
    }
}
