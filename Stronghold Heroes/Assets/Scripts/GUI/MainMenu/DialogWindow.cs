﻿using UnityEngine;

public class DialogWindow : MonoBehaviour 
{
    public delegate void OnOptionPressed();

    private OnOptionPressed onYesPressed;
    private OnOptionPressed onNoPressed;

    public void Init(OnOptionPressed onYesPressed, OnOptionPressed onNoPressed = null)
    {
        this.onYesPressed = onYesPressed;
        this.onNoPressed = onNoPressed;
    }

	public void OnYesPressed()
    {
        if(onYesPressed != null)
        {
            onYesPressed();
        }
    }

    public void OnNoPressed()
    {
        if(onNoPressed != null)
        {
            onNoPressed();
        }
        else
        {
            EventManager.Instance.OnHideDialogWindow();
        }
    }
}
