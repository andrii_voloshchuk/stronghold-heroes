﻿using UnityEngine;
using UnityEngine.UI;

public class OptionsGUIController : MonoBehaviour 
{
    public Toggle musicToggle;
    public Toggle soundToggle;

    public void Init()
    {
        musicToggle.isOn = MusicManager.Instance.IsEnabled;
        soundToggle.isOn = SoundManager.Instance.IsEnabled;
    }

    public void OnMusicToggle()
    {
        MusicManager.Instance.SwitchMusic(musicToggle.isOn);
    }

    public void OnSoundToggle()
    {
        SoundManager.Instance.SwitchSound(soundToggle.isOn);
    }
}
