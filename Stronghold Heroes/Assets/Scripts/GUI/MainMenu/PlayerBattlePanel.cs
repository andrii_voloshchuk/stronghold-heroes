﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PlayerBattlePanel : MonoBehaviour 
{
    [Serializable]
    public class UnitBattleButton
    {
        private Unit unit;
        public Button button;
        public Text level;

        private int index;
        private int unitIndex;

        private Player player;

        public void Init(Player player, int unitIndex)
        {
            this.player = player;
            this.unitIndex = unitIndex;
            button.onClick.AddListener(delegate { ChangeUnit(); });
        }

        private void ChangeUnit()
        {
            index++;
            if (index > 9)
                index = 0;

            unit = GameHelper.GetRandomUnits(1, index)[0];
            player.units[unitIndex] = new Unit(unit);
            SetUnit(unit);
        }

        public void SetUnit(Unit unit)
        {
            index = (int)unit.unitType;
            this.unit = new Unit(unit);
            button.image.sprite = SpriteDictionary.Instance.GetUnitSprite(unit.unitType);
            level.text = unit.level.ToString();
        }


    }

    public List<UnitBattleButton> unitButtons;

    public PlayerAvatar avatar;

    private Player player;

    public Slider slider;

    public void Init(Player player)
    {
        this.player = player;
        avatar.Init(player.avatar);
        for (int i = 0; i < player.units.Count; i++)
        {
            unitButtons[i].Init(player, i);
            unitButtons[i].SetUnit(player.units[i]);
        }
    }
	
    public void RandomizeUnits()
    {
        player.units = GameHelper.GetRandomUnits(6);
        for (int i = 0; i < player.units.Count; i++)
        {
            unitButtons[i].SetUnit(player.units[i]);
        }
    }

    public void ChangeLevels()
    {
        for (int i = 0; i < player.units.Count; i++)
        {
            player.units[i] = GameHelper.UpgradeUnit(player.units[i], (int)slider.value);
            unitButtons[i].SetUnit(player.units[i]);
        }
    }

    public void RandomizeLevel()
    {
        int random = GameHelper.GetRandomInt(1, 50, true);
        for (int i = 0; i < player.units.Count; i++)
        {
            player.units[i] = GameHelper.UpgradeUnit(player.units[i], random);
            unitButtons[i].SetUnit(player.units[i]);
        }
        slider.value = random;
    }
}
