﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class PlayerAvatar : MonoBehaviour 
{
    private int index;
    private int count;

    private Image image;

    public bool setAvatarName;

    public Text avatarName;

    private void Awake()
    {
        image = GetComponent<Image>();
        count = Enum.GetNames(typeof(AVATAR)).Length;
    }

	public void Init(AVATAR avatar)
    {
        index = (int)avatar;
        SetAvatart(index);
    }
	
    private void SetAvatart(int index)
    {
        AVATAR avatar = (AVATAR)index;
        image.sprite = SpriteDictionary.Instance.GetAvatarSprite(avatar);
        if(setAvatarName && avatarName != null)
        {
            avatarName.text = TextDictionary.GetText(avatar.ToString());
        }
    }

    public void OnChangeAvatar(int value)
    {
        index += value;

        if (index < 0)
            index = count - 1;
        else if (index >= count)
            index = 0;
        SetAvatart(index);
    }

    public AVATAR GetSelectedAvatar()
    {
        return (AVATAR)index;
    }
}
