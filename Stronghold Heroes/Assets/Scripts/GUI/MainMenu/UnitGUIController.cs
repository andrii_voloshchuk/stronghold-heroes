﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class UnitGUIController : MonoBehaviour 
{
    [Serializable]
    public struct Stat
    {
        public STAT_NAME statName;
        public GameObject gameObject;
        public Text name;
        public Text amount;
        public Text plus;
    }

    public Text gold;
    public Text cost;
    [Space(8)]
    public Text unitName;
    public Text unitLevel;
    [Space(8)]
    public Text specialPowerName;
    public Text specialPowerDescription;

    [Space(8)]
    public Image unitImage;

    public Button upgradeButton;

    [Space(8)]
    public List<Stat> stats;

    private Dictionary<STAT_NAME, Stat> statsDictionary;

    private UnitsInitialData uid;

    private Unit unit;
    private Unit data;

    private int index;


    private void Awake()
	{
        statsDictionary = new Dictionary<STAT_NAME, Stat>();
        foreach(Stat stat in stats)
        {
            statsDictionary.Add(stat.statName, stat);
        }
        LoadInitialData();
	}

    private void LoadInitialData()
    {
        uid = GameHelper.GetInitialData();
    }

    private float WrapToPercent(float value, bool isInPercent)
    {
        return (value * (isInPercent ? 100f : 1f));
    }

	public void UpdateStats(Unit unit, int index)
    {
        this.unit = unit;
        this.index = index;

        statsDictionary[STAT_NAME.PRIMARY].gameObject.SetActive(true);
        statsDictionary[STAT_NAME.SECONDARY].gameObject.SetActive(true);


        unitImage.sprite = SpriteDictionary.Instance.GetUnitSprite(unit.unitType);


        int goldAmount = PlayerProfile.GetGold();
        int costAmount = unit.level * 100;

        gold.text = goldAmount.ToString();
        cost.text = costAmount.ToString();

        upgradeButton.interactable = goldAmount >= costAmount && unit.level < 50;

        unitName.text = TextDictionary.GetText(unit.unitType.ToString());
        unitLevel.text = "lvl " + unit.level;

        specialPowerName.text = "Special Power: <color=red>" + TextDictionary.GetText(unit.specialPower.specialPowerType.ToString()) + "</color>";
        specialPowerDescription.text = TextDictionary.GetText(unit.specialPower.specialPowerType + ".DESCRIPTION", new string[] { unit.specialPower.duration.ToString() });

        statsDictionary[STAT_NAME.HEALTH].amount.text = ((int)unit.hp).ToString();
        statsDictionary[STAT_NAME.HEALTH].plus.text = unit.level >= 50 ? "MAX" : "+" + (int)GameHelper.GetStatPlus(STAT_NAME.HEALTH, unit.level);

        statsDictionary[STAT_NAME.DAMAGE].amount.text = ((int)unit.damage).ToString();
        statsDictionary[STAT_NAME.DAMAGE].plus.text = unit.level >= 50 ? "MAX" : "+" + (int)GameHelper.GetStatPlus(STAT_NAME.DAMAGE, unit.level);

        statsDictionary[STAT_NAME.RANGE].amount.text = unit.range.ToString();
        string range = unit.range >= Map.GRID_WIDTH || unit.level >= 50 ? "MAX" : "+" + (int)GameHelper.GetStatPlus(STAT_NAME.RANGE, unit.level);
        statsDictionary[STAT_NAME.RANGE].plus.text = range;

        string cooldown = unit.specialPower.coolDown <= 1 || unit.level >= 50 ? "MAX" : "-" + (int)GameHelper.GetStatPlus(STAT_NAME.COOLDOWN, unit.level);
        statsDictionary[STAT_NAME.COOLDOWN].amount.text = unit.specialPower.coolDown.ToString();
        statsDictionary[STAT_NAME.COOLDOWN].plus.text = cooldown;

        data = uid.units.Find(i => i.unitType == unit.unitType);

        if(data.specialPower.primaryParameter > 0)
        {
            statsDictionary[STAT_NAME.PRIMARY].name.text = TextDictionary.GetText(unit.specialPower.specialPowerType + ".PRIMARY");
            bool isInPercent = data.specialPower.primaryParameter < 1f;
            statsDictionary[STAT_NAME.PRIMARY].amount.text =
                 (int)WrapToPercent(unit.specialPower.primaryParameter, isInPercent) + (isInPercent ? "%" : string.Empty);
            statsDictionary[STAT_NAME.PRIMARY].plus.text = unit.level >= 50 ? "MAX" :
                "+" + WrapToPercent(GameHelper.GetStatPlus(STAT_NAME.PRIMARY, unit.level, isInPercent), isInPercent) +(isInPercent ? "%" : string.Empty);
        }
        else
        {
            statsDictionary[STAT_NAME.PRIMARY].gameObject.SetActive(false);
        }

        if (data.specialPower.secondaryParameter > 0)
        {
            statsDictionary[STAT_NAME.SECONDARY].name.text = TextDictionary.GetText(unit.specialPower.specialPowerType + ".SECONDARY");
            bool isInPercent = data.specialPower.secondaryParameter < 1f;
            statsDictionary[STAT_NAME.SECONDARY].amount.text =
                 (int)WrapToPercent(unit.specialPower.secondaryParameter, isInPercent) + (isInPercent ? "%" : string.Empty);
            statsDictionary[STAT_NAME.SECONDARY].plus.text = unit.level >= 50 ? "MAX" :
                "+" + WrapToPercent(GameHelper.GetStatPlus(STAT_NAME.SECONDARY, unit.level, isInPercent), isInPercent) + (isInPercent ? "%" : string.Empty);
        }
        else
        {
            statsDictionary[STAT_NAME.SECONDARY].gameObject.SetActive(false);
        }

    }

    public void OnUpgrade()
    {
        int goldAmount = PlayerProfile.GetGold();
        int costAmount = unit.level * 100;

        if (goldAmount >= costAmount)
        {
            unit = GameHelper.UpgradeUnit(unit);
      
            PlayerProfile.AddGold(-costAmount);
            PlayerProfile.UpgradeUnit(unit, index);
            PlayerProfile.Save();
        }
        UpdateStats(unit, index);
    }
}
