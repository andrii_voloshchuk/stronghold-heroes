﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class InstantBattleGUIController : MonoBehaviour 
{
    private enum OPTION
    {
        PLAYER_VS_PLAYER = 0,
        PLAYER_VS_BOT = 1,
        BOT_VS_BOT = 2
    }

    public PlayerBattlePanel player1BattlePanel;
    public PlayerBattlePanel player2BattlePanel;

    private Player player1;
    private Player player2;

    public void Init(int option)
    {
        OPTION opt = (OPTION)option;

        player1 = GenerateRandomPlayer();
        player2 = GenerateRandomPlayer();

        if(opt == OPTION.PLAYER_VS_BOT)
        {
            player2.isAi = true;
        }
        else if(opt == OPTION.BOT_VS_BOT)
        {
            player1.isAi = true;
            player2.isAi = true;
        }

        player1BattlePanel.Init(player1);
        player2BattlePanel.Init(player2);
    }

    private Player GenerateRandomPlayer()
    {
        var allAvatars = Enum.GetValues(typeof(AVATAR));
        List<AVATAR> avatars = new List<AVATAR>();

        foreach (AVATAR avatar in allAvatars)
        {
            if (avatar != AVATAR.CRUSADER && avatar != AVATAR.ARAB)
            {
                avatars.Add(avatar);
            }
        }

        int random = GameHelper.GetRandomInt(0, avatars.Count);
        Player player = new Player(TextDictionary.GetText(avatars[random].ToString()));
        player.avatar = avatars[random];
        player.units = GameHelper.GetRandomUnits(6);
        return player;
    }


    public void OnStart()
    {
        AVATAR avatar1 = player1BattlePanel.avatar.GetSelectedAvatar(); 
        AVATAR avatar2 = player2BattlePanel.avatar.GetSelectedAvatar();
        player1.avatar = avatar1;
        player2.avatar = avatar2;
        player1.name = TextDictionary.GetText(avatar1.ToString());
        player2.name = TextDictionary.GetText(avatar2.ToString());
        Core.Instance.StartBattle(GAME_MODE.INSTANT, player1, player2);
    }
}
