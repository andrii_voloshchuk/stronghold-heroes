﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ProfileGUIController : MonoBehaviour 
{
    private const string UNIT_BUTTON_PATH = "GUI/UnitButton";

    public PlayerAvatar playerAvatar;

    private List<UnitButton> unitButtons;

    public InputField nameInputField;

    public Text goldText;

    public RectTransform unitsPlacement;

    public Button buyButton;

    private Player player;

    private void Awake()
    {
        EventManager.onBackPressed += OnBack;
        unitButtons = new List<UnitButton>();
    }

    private void OnDestroy()
    {
        EventManager.onBackPressed -= OnBack;
    }

    public void UpdateData() 
	{
        
        if(unitButtons.Count > 0)
        {
            DestroyUnitButtons();
        }

        player = PlayerProfile.GetPlayer();

        nameInputField.placeholder.GetComponent<Text>().text = player.name;
        playerAvatar.Init(player.avatar);

        int cost = 1000;
        int gold = PlayerProfile.GetGold();

        buyButton.interactable = gold >= cost;

        goldText.text = gold.ToString();

        CreateUnitButtons(player);

    }

    private void CreateUnitButtons(Player player)
    {
        float startPosX = 50f;
        float startPosY = -50f;

        float spacing = 100f;

        int elementsPerRow = 6;
        int index = 0;
        int row = 0;

        for(int i = 0; i < player.units.Count; i++)
        {
            if (index >= elementsPerRow)
            {
                index = 0;
                row++;
            }
            UnitButton ub = ResourcesManager.InstantiateGameObject<UnitButton>(UNIT_BUTTON_PATH);
            ub.transform.SetParent(unitsPlacement);

            RectTransform rt = ub.GetComponent<RectTransform>();
            float posX = startPosX + spacing * index;
            float posY = startPosY - spacing * row;
            rt.anchoredPosition = new Vector2(posX, posY);
            rt.localScale = Vector3.one;

            ub.SetUnit(player.units[i], i);
            unitButtons.Add(ub);
            index++;
        }

        unitsPlacement.sizeDelta = new Vector2(unitsPlacement.sizeDelta.x, spacing * (row + 1) + spacing);
    }

    private void DestroyUnitButtons()
    {
        for (int i = unitButtons.Count - 1; i >= 0; i--)
        {
            Destroy(unitButtons[i].gameObject);
        }
        unitButtons.Clear();
    }
	
    public void OnBack()
    {
        if (gameObject.activeSelf)
        {
            if (nameInputField.text.Length > 2)
            {
                player.name = nameInputField.text;
            }
            player.avatar = playerAvatar.GetSelectedAvatar();

            PlayerProfile.Update(player);
            PlayerProfile.Save();
            MainMenuGUIController.Instance.BackToMenu();
        }
    }

    public void BuyUnit()
    {
        int cost = 1000;
        int gold = PlayerProfile.GetGold();
        if (gold >= cost)
        {
            Unit unit = GameHelper.GetRandomUnits(1)[0];
            PlayerProfile.AddUnit(unit);
            PlayerProfile.Save();
            UpdateData();
        }
    }
   
}
