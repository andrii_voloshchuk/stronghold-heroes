﻿using UnityEngine;
using UnityEngine.UI;

public class InputWindow : MonoBehaviour
{
    public InputField nameTitle;
    public Button startGameButton;

    public void Init()
    {
        nameTitle.onValueChanged.AddListener(delegate { CheckName(); });
    }

    private void CheckName()
    {
        string playerName = nameTitle.text;
        startGameButton.interactable = playerName.Length > 2;
    }

    public void OnStartGameClick()
    {
        string playerName = nameTitle.text;
        if (playerName.Length > 3)
        {
            PlayerProfile.Create(playerName);
            MainMenuGUIController.Instance.OnStartGame();
        }
    }
}
