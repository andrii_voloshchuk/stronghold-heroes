﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class UnitButton : MonoBehaviour 
{
    public Text textName;
    public Text textLevel;

    private Button button;

    private Unit unit;

    private int index;

    private void Awake()
    {
        button = GetComponent<Button>();
    }

    public void SetUnit(Unit unit, int index)
    {
        this.index = index;
        this.unit = unit;
        Sprite sprite = SpriteDictionary.Instance.GetUnitSprite(unit.unitType);
        button.image.sprite = sprite;
        textName.text = GameHelper.GetCleanUnitName(unit.unitType);
        textLevel.text = "lvl " + unit.level;
    }

    public void OnClick()
    {
        MainMenuGUIController.Instance.OpenUnit(unit, index);
    }
}
