﻿using UnityEngine;
using UnityEngine.UI;

public class SmallWindow : MonoBehaviour 
{
    public enum TITLE
    {
        INPUT_NAME,
        QUIT_GAME
    }

    public GameObject mainWindow;

    public InputWindow inputWindow;

    public DialogWindow dialogWindow;

    public Text smallWindowTitle;

    private void Awake()
    {
        EventManager.onShowInputWindow += ShowInputWindow;
        EventManager.onShowDialogWindow += ShowDialogWindow;
        EventManager.onHideInputWindow += HideInputWindow;
        EventManager.onHideDialogWindow += HideDialogWindow;
    }

    private void OnDestroy()
    {
        EventManager.onShowInputWindow -= ShowInputWindow;
        EventManager.onShowDialogWindow -= ShowDialogWindow;
        EventManager.onHideInputWindow -= HideInputWindow;
        EventManager.onHideDialogWindow -= HideDialogWindow;
    }

    private void SetMainWindowActive(bool value)
    {
        mainWindow.SetActive(value);
    }

    private void ShowInputWindow()
    {
        SetMainWindowActive(true);
        inputWindow.gameObject.SetActive(true);
        inputWindow.Init();
        smallWindowTitle.text = TextDictionary.GetText(TITLE.INPUT_NAME.ToString());
    }

    private void HideInputWindow()
    {
        SetMainWindowActive(false);
        inputWindow.gameObject.SetActive(false);
        smallWindowTitle.text = string.Empty;
    }

    private void ShowDialogWindow(TITLE title, DialogWindow.OnOptionPressed onYesPressed, DialogWindow.OnOptionPressed onNopressed)
    {
        SetMainWindowActive(true);
        dialogWindow.gameObject.SetActive(true);
        smallWindowTitle.text = TextDictionary.GetText(title.ToString());
        dialogWindow.Init(onYesPressed, onNopressed);
    }

    private void HideDialogWindow()
    {
        SetMainWindowActive(false);
        dialogWindow.gameObject.SetActive(false);
        smallWindowTitle.text = string.Empty;
    }

}
