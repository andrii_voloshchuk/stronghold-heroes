﻿using UnityEngine;
using System.Collections.Generic;

public class MainMenuGUIController : MonoBehaviour 
{
    public enum CATEGORY
    {
        MAIN,
        PROFILE,
        UNIT,
        OPTIONS,
        INSTANT_BATTLE,
        INSTANT_BATTLE_CONFIG
    }

    private struct Category
    {
        public GameObject background;
        public GameObject ui;

        public Category(GameObject background, GameObject ui)
        {
            this.background = background;
            this.ui = ui;
        }

        public void SetActive(bool value)
        {
            background.SetActive(value);
            ui.SetActive(value);
        }
    }

    [Header("Controllers")]

    public GameObject menuButtons;
    public GameObject instantBattleButtons;

    public ProfileGUIController profileGUIController;

    public UnitGUIController unitGUIController;

    public OptionsGUIController optionsGUIController;

    public InstantBattleGUIController instantBattleGUIController;

    [Space(8)]
    [Header("Backgrounds")]
    public GameObject mainMenuBackground;
    public GameObject profileBackground;
    public GameObject unitBackground;
    public GameObject battleBackGround;


    private static MainMenuGUIController instance;
    public static MainMenuGUIController Instance { get { return instance; } }

    private Dictionary<CATEGORY, Category> categories;

    private CATEGORY currentCategory;
    private CATEGORY previousCategory;

    private static bool startSoundPlayed;

	private void Awake()
	{
        if (instance != null & instance != this)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            instance = this;
        }

        EventManager.onBackPressed += OnBackPressed;
        EventManager.onSceneLoad += EventManager_onSceneLoad;
        MusicManager.Instance.PlayOneClip("MainMenu");

        categories = new Dictionary<CATEGORY, Category>();
        categories.Add(CATEGORY.MAIN, new Category(mainMenuBackground, menuButtons));
        categories.Add(CATEGORY.PROFILE, new Category(profileBackground, profileGUIController.gameObject));
        categories.Add(CATEGORY.UNIT, new Category(unitBackground, unitGUIController.gameObject));
        categories.Add(CATEGORY.OPTIONS, new Category(unitBackground, optionsGUIController.gameObject));
        categories.Add(CATEGORY.INSTANT_BATTLE, new Category(battleBackGround, instantBattleButtons));
        categories.Add(CATEGORY.INSTANT_BATTLE_CONFIG, new Category(battleBackGround, instantBattleGUIController.gameObject));

        if (!PlayerProfile.Exists)
        {
            EventManager.Instance.OnShowInputWindow();
        }
        else
        {
            PlayerProfile.Load();
            OnStartGame();
        }
    }


    private void OnDestroy()
    {
        EventManager.onBackPressed -= OnBackPressed;
        EventManager.onSceneLoad -= EventManager_onSceneLoad;
    }

    private void UpdateCategory(CATEGORY category)
    {
        previousCategory = currentCategory;
        currentCategory = category;
        foreach (var entry in categories)
        {
            entry.Value.SetActive(false);
        }
        categories[category].SetActive(true);
    }


    public void OnStartGame()
    {
        EventManager.Instance.OnHideInputWindow();
        menuButtons.SetActive(true);
        if (!startSoundPlayed)
        {
            startSoundPlayed = true;
            SoundManager.Instance.PlayOneSound("startgame");
        }
    }

    public void BackToMenu()
    {
        UpdateCategory(CATEGORY.MAIN);
    }

    public void OpenProfile()
    {
        UpdateCategory(CATEGORY.PROFILE);
        profileGUIController.UpdateData();
    }

    public void OpenUnit(Unit unit, int index)
    {
        UpdateCategory(CATEGORY.UNIT);
        unitGUIController.UpdateStats(unit, index);
    }

    public void OpenOptions()
    {
        UpdateCategory(CATEGORY.OPTIONS);
        optionsGUIController.Init();
    }

    public void OnQuit()
    {
        SoundManager.Instance.PlayOneSound("quitgame");
        EventManager.Instance.OnShowDialogWindow(SmallWindow.TITLE.QUIT_GAME, delegate { Quit(); });
    }

    private void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void OpenInstantBattle()
    {
        UpdateCategory(CATEGORY.INSTANT_BATTLE);
    }

    public void OpenInstantBattleConfig(int option)
    {
        UpdateCategory(CATEGORY.INSTANT_BATTLE_CONFIG);
        instantBattleGUIController.Init(option);
    }

    private void OnBackPressed()
    {
        if (currentCategory == CATEGORY.MAIN)
        {
            OnQuit();
        }
        else
        {
            OnBack(previousCategory);
        }
    }


    private void OnBack(CATEGORY category)
    {
        switch(category)
        {
            case CATEGORY.MAIN:
            case CATEGORY.UNIT:
            case CATEGORY.INSTANT_BATTLE_CONFIG:
                BackToMenu();
                break;
            case CATEGORY.PROFILE:
                OpenProfile();
                break;
            case CATEGORY.INSTANT_BATTLE:
                OpenInstantBattle();
                break;
        }
    }

    public void LoadCrusaderPath()
    {
        Core.Instance.LoadScene("Map");
    }

    private void EventManager_onSceneLoad()
    {
        foreach (var entry in categories)
        {
            entry.Value.SetActive(false);
        }
    }
}
