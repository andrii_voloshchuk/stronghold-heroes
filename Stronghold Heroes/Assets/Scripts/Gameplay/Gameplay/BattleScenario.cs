﻿using System.Collections.Generic;

public class BattleScenario 
{
    private int actionIndex;
    private int turnIndex;

    private Queue<BattleAction> battleActions;

    private Player player;

    public BattleScenario()
    {
        actionIndex = 0;
        battleActions = new Queue<BattleAction>();
    }

    public void OnTurnStarted(Player player)
    {
        turnIndex++;
        Log.Battle("Turn started: " + player.name + " with index " + turnIndex);
        this.player = player;
    }

    public void OnTurnFinished(int id)
    {
        Log.Battle("Turn finished: " + player.name + " with index " + turnIndex + " and unit id " + id);
        actionIndex = 0;
        Gameplay.Instance.OnTurnFinished(player, id);
    }

    public void OnActionFinished(int initiatorId, ACTION_TYPE actionType)
    {
        Log.Battle("Action " + actionType + " finished: " + player.name + " with index " + turnIndex + " and unit id " + initiatorId);

        battleActions.Dequeue();
        if(battleActions.Count > 0)
        {
            ProcessAction();
        }
        else
        {
            OnTurnFinished(initiatorId);
        }
    }

    public void OnActionStarted(int initiatorId, ACTION_TYPE actionType, BattleAction.SimpleAction action)
    {
        Log.Battle("Action " + actionType + " started: " + player.name + " with index " + turnIndex + " and unit id " + initiatorId);

        BattleAction battleAction = new BattleAction(turnIndex, actionIndex, initiatorId, actionType, action);
        battleActions.Enqueue(battleAction);
        actionIndex++;
    }

    public void ProcessAction()
    {
        BattleAction battleAction = battleActions.Peek();
        Log.Battle("Action " + battleAction.actionType + " processing: " + player.name + " with index " + turnIndex + " and unit id " + battleAction.initiatorId);


        switch(battleAction.actionType)
        {
            case ACTION_TYPE.ATACK:
                ProcessAttackAction(battleAction);
                break;
            case ACTION_TYPE.DEATH:
                ProcessDeathAction(battleAction);
                break;
            case ACTION_TYPE.MOVE:
                ProcessMoveAction(battleAction);
                break;
            case ACTION_TYPE.SPECIAL_POWER:
                ProcessSpecialPower(battleAction);
                break;
        }

    }

    private void ProcessMoveAction(BattleAction action)
    {
        BattleAction.MoveAction moveAction = (BattleAction.MoveAction)action.action;
        Gameplay.Instance.MoveUnit(action.initiatorId, moveAction.destination);
    }

    private void ProcessAttackAction(BattleAction action)
    {
        BattleAction.AttackAction attackAction = (BattleAction.AttackAction)action.action;
        Gameplay.Instance.AttackUnits(action.initiatorId, attackAction.damagedUnit);
    }

    private void ProcessDeathAction(BattleAction action)
    {
        BattleAction.DeathAction deathAction = (BattleAction.DeathAction)action.action;
        Gameplay.Instance.KillUnit(deathAction.victimId, action.initiatorId);
    }

    private void ProcessSpecialPower(BattleAction action)
    {
        BattleAction.SpecialPowerAction specialPowerAction = (BattleAction.SpecialPowerAction)action.action;
        Gameplay.Instance.AffectWithSpecialPower(action.initiatorId, specialPowerAction.specialPowerType, specialPowerAction.affectedUnit);
    }
}
