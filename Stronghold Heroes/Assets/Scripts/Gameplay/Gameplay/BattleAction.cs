﻿using System;

[Serializable]
public class BattleAction
{
    [Serializable]
    public struct UnitDamageData
    {
        public int id;
        public float damage;

        public UnitDamageData(int id, float damage)
        {
            this.id = id;
            this.damage = damage;
        }
    }

    [Serializable]
    public struct UnitSpecialPowerData
    {
        public int id;
        public float primaryParameter;
        public float secondaryParameter;
        public int duration;

        public UnitSpecialPowerData(int id, int duration, float primaryParameter, float secondaryParameter)
        {
            this.id = id;
            this.duration = duration;
            this.primaryParameter = primaryParameter;
            this.secondaryParameter = secondaryParameter;
        }
    }

    public abstract class SimpleAction
    {
    }

    [Serializable]
    public class MoveAction : SimpleAction
    {
        public MapVector destination;

        public MoveAction(MapVector destination)
        {
            this.destination = destination;
        }
    }

    [Serializable]
    public class AttackAction : SimpleAction
    {
        public UnitDamageData damagedUnit;

        public AttackAction()
        {
        }

        public AttackAction(UnitDamageData damagedUnit)
        {
            this.damagedUnit = damagedUnit;
        }
    }

    [Serializable]
    public class SpecialPowerAction : SimpleAction
    {
        public SPECIAL_POWER_TYPE specialPowerType;

        public UnitSpecialPowerData affectedUnit;

        public SpecialPowerAction(SPECIAL_POWER_TYPE specialPowerType, UnitSpecialPowerData affectedUnit)
        {
            this.specialPowerType = specialPowerType;
            this.affectedUnit = affectedUnit;
        }
    }

    [Serializable]
    public class DeathAction : SimpleAction
    {
        public int victimId;

        public DeathAction(int victimId)
        {
            this.victimId = victimId;
        }
    }

    public ACTION_TYPE actionType;

    public int initiatorId;

    public SimpleAction action;

    public int actionId;

    public int turnId;

    public BattleAction(int turnId, int actionId, int initiatorId, ACTION_TYPE actionType, SimpleAction action)
    {
        this.turnId = turnId;
        this.actionId = actionId;
        this.initiatorId = initiatorId;
        this.actionType = actionType;
        this.action = action;
    }
}
