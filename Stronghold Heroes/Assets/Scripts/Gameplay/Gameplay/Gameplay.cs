﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;

public class Gameplay : MonoBehaviour 
{
    private static Gameplay instance;
    public static Gameplay Instance { get { return instance; } }

    public Map map;

    public PanelTurn panelTurn;

    public PanelTop panelTop;

    public SpecialPowerButton specialPowerButton;

    public Button skipButton;

    public Text turnText;

    public GameObject levelGUI;

    [Space(8)]
    [Header("Win")]
    public GameObject winGUI;
    public GameObject instantGUI;

    public WinGUI campaignWin;

    public PanelTop.PlayerAvatar winAvatar;

    [Space(8)]
    [Header("Controllers")]
    public PlayerController playerController1;
    public PlayerController playerController2;

    public AIContorller aiController1;
    public AIContorller aiController2;

    private Player player1;
    private Player player2;

    private Dictionary<Player, BaseController> players;

    private List<UnitTurn> turnList;

    private int unitIndexer = 0;

    private BattleScenario battleScenario;

    private UnitTurn currentTurn;

    private void Awake()
    {
        if (instance != null & instance != this)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            instance = this;
        }

        EventManager.onStartBattleAction += EventManager_onStartBattleAction;
        EventManager.onFinishBattleAction += EventManager_onFinishBattleAction;
        EventManager.onProcessBattleAction += EventManager_onProcessBattleAction;
        EventManager.onBackPressed += OnBack;
        EventManager.onSceneLoad += EventManager_onSceneLoad;

        battleScenario = new BattleScenario();
        map.Init();
        InitBattle();
        InitTurnList();
    }

    private void Start()
    {
        MusicManager.Instance.PlayRandomClip("Battle", 1, 4);
    }

    private void OnDestroy()
    {
        EventManager.onStartBattleAction -= EventManager_onStartBattleAction;
        EventManager.onFinishBattleAction -= EventManager_onFinishBattleAction;
        EventManager.onProcessBattleAction -= EventManager_onProcessBattleAction;
        EventManager.onBackPressed -= OnBack;
        EventManager.onSceneLoad -= EventManager_onSceneLoad;
    }


    public void OnTurnStarted(int id)
    {
        BlockSpecialPowerButton(true);
        BlockSkipButton(true);
        StartCoroutine(OnTurnStartedDelay(1f, id));
    }

    private IEnumerator OnTurnStartedDelay(float delay, int id)
    {
        yield return new WaitForSeconds(delay);
        BlockSpecialPowerButton(false);
        BlockSkipButton(false);
        UnitTurn unitTurn = GetUnitTurnById(id);
        if (turnList.Contains(unitTurn))
        {
            SetTurn(unitTurn);
            currentTurn = unitTurn;
            Unit unit = currentTurn.Unit;
            Player player = currentTurn.Player;
            if (!CheckWin(player))
            {
                battleScenario.OnTurnStarted(player);
                specialPowerButton.UpdateSpecialPower(unit.specialPower.specialPowerType, unit.specialPower.GetCurrentCoolDown());
                players[player].TakeTurn(unit);
            }
            else
            {
                ShowWinGUI(player);
            }
        }
    }

    public void OnTurnFinished(Player player, int id)
    {
        UnitTurn unitTurn = GetUnitTurnById(id);
        if (turnList.Contains(unitTurn))
        {
            players[player].FinishTurn(unitTurn.Unit);
            turnList.Remove(unitTurn);
            turnList.Add(unitTurn);
            panelTurn.UpdateTurnList(turnList);
            OnTurnStarted(turnList[0].Unit.Id);
        }
    }

    private void ShowWinGUI(Player player)
    {
        levelGUI.SetActive(false);
        winGUI.SetActive(true);
        GAME_MODE mode = Core.Instance.gameMode;

        if (mode == GAME_MODE.INSTANT)
        {
            instantGUI.SetActive(true);
            winAvatar.SetAvatar(player);
        }
        else if (mode == GAME_MODE.CAMPAIGN)
        {
            bool win = player == player1;
            if (win)
            {
                PlayerProfile.UpdateLevel(Core.Instance.level);
                PlayerProfile.Save();
            }
            campaignWin.gameObject.SetActive(true);
            campaignWin.Init(player1, player2, win);
        }
    }


    private bool CheckWin(Player player)
    {
        int count = 0;
        List<Unit> enemies = GetAllEnemyUnits(player);
        foreach (Unit unit in enemies)
        {
            if (unit.IsDead)
                count++;
        }
        return count >= enemies.Count;
    }

    private void SetTurn(UnitTurn turn)
    {
        if (currentTurn == null || currentTurn.Player != turn.Player)
        {
            turnText.gameObject.SetActive(true);
            turnText.text = turn.Player.name + "'s turn";
            StartCoroutine(DisactiveTurnText(1f));
        }
    }

    private IEnumerator DisactiveTurnText(float delay)
    {
        yield return new WaitForSeconds(delay);
        turnText.gameObject.SetActive(false);
    }

    private void EventManager_onStartBattleAction(int initiatorId, ACTION_TYPE type, BattleAction.SimpleAction action)
    {
        battleScenario.OnActionStarted(initiatorId, type, action);
    }

    private void EventManager_onFinishBattleAction(int initiatorId, ACTION_TYPE type)
    {
        battleScenario.OnActionFinished(initiatorId, type);
    }

    private void EventManager_onProcessBattleAction()
    {
        battleScenario.ProcessAction();
    }

    private void InitBattle()
    {
        players = new Dictionary<Player, BaseController>();
        
        UNIT_TEAM team1 = (UNIT_TEAM)Random.Range(0, 2);
        int position1 = team1 == UNIT_TEAM.BLUE ? 0 : Map.GRID_WIDTH - 1;
        UNIT_TEAM team2 = team1 == UNIT_TEAM.BLUE ? UNIT_TEAM.RED : UNIT_TEAM.BLUE;
        int position2 = team2 == UNIT_TEAM.BLUE ? 0 : Map.GRID_WIDTH - 1;

        Player[] currentPlayers = Core.Instance.GetPlayers();
        player1 = currentPlayers[0];
        player2 = currentPlayers[1];
        player1.unitTeam = team1;
        player2.unitTeam = team2;

        for (int i = 0; i < player1.units.Count; i++)
        {
            player1.units[i].UnitTeam = team1;
            player1.units[i].Id = unitIndexer;
            unitIndexer++;
        }

        for (int i = 0; i < player2.units.Count; i++)
        {
            player2.units[i].UnitTeam = team2;
            player2.units[i].Id = unitIndexer;
            unitIndexer++;
        }

        Player[] turnPlayers = position1 == 0 ? new Player[] { player1, player2 } : new Player[] { player2, player1 };
        panelTop.Init(turnPlayers);

        if (player1.isAi)
        {
            aiController1.Init();
            aiController1.InstantiateUnits(player1, position1);
            players.Add(player1, aiController1);
        }
        else
        {
            playerController1.Init();
            playerController1.InstantiateUnits(player1, position1);
            players.Add(player1, playerController1);
        }

        if(player2.isAi)
        {
            aiController2.Init();
            aiController2.InstantiateUnits(player2, position2);
            players.Add(player2, aiController2);
        }
        else
        {
            playerController2.Init();
            playerController2.InstantiateUnits(player2, position2);
            players.Add(player2, playerController2);
        }
       
    }

    private void InitTurnList()
    {
        turnList = new List<UnitTurn>();
        for (int i = 0; i < player1.units.Count; i++)
        {
            turnList.Add(new UnitTurn(player1, player1.units[i], player1.unitTeam));
        }
        for (int i = 0; i < player2.units.Count; i++)
        {
            turnList.Add(new UnitTurn(player2, player2.units[i], player2.unitTeam));
        }
        panelTurn.UpdateTurnList(turnList);
        OnTurnStarted(turnList[0].Unit.Id);
    }

    private Player GetPlayerByUnit(Unit unit)
    {
        if (player1.units.Contains(unit))
            return player1;
        return player2;
    }

    private UnitTurn GetUnitTurnById(int id)
    {
        foreach (UnitTurn unitTurn in turnList)
        {
            if(unitTurn.UnitId == id)
            {
                return unitTurn;
            }
        }
        return null;
    }

    public List<Unit> GetAllEnemyUnits(Player player)
    {
        Player other = player1 == player ? player2 : player1;
        return GetAllPlayerUnits(other);
    }

    public List<Unit> GetAllAllyUnits(Player player)
    {
        return GetAllPlayerUnits(player);
    }

    private List<Unit> GetAllPlayerUnits(Player player)
    {
        List<Unit> unitsList = new List<Unit>();
        foreach (UnitTurn unitTurn in turnList)
        {
            if(unitTurn.Player == player)
            {
                unitsList.Add(unitTurn.Unit);
            }
        }
        return unitsList;
    }

    public void MoveUnit(int id, MapVector destination)
    {
        UnitTurn unitTurn = GetUnitTurnById(id);
        if (turnList.Contains(unitTurn))
        {
            BaseController bc = players[unitTurn.Player];
            bc.MoveUnit(unitTurn.Unit, destination);
        }
        else
        {
            Debug.LogError("Turn list does'n contain unit with id " + id);
        }
    }

    public void AttackUnits(int id, BattleAction.UnitDamageData damagedUnit)
    {
        UnitTurn unitTurn = GetUnitTurnById(id);
        Unit victim = GetUnitTurnById(damagedUnit.id).Unit;
        if (turnList.Contains(unitTurn))
        {
            BaseController attacker = players[unitTurn.Player];
            attacker.AttackUnit(unitTurn.Unit, victim);
            BaseController player = players[GetPlayerByUnit(victim)];
            player.DamageUnit(unitTurn.Unit, victim, damagedUnit.damage);
        }
        else
        {
            Debug.LogError("Turn list does'n contain unit with id " + id);
        }
    }

    public void KillUnit(int id, int killerId)
    {
        UnitTurn unitTurn = GetUnitTurnById(id);
        Unit killer = GetUnitTurnById(killerId).Unit;
        if (turnList.Contains(unitTurn))
        {
            turnList.Remove(unitTurn);
            panelTurn.UpdateTurnList(turnList);
            BaseController bc = players[unitTurn.Player];
            bc.KillUnit(unitTurn.Unit, killer);
        }
        else
        {
            Debug.LogError("Turn list does'n contain unit with id " + id);
        }
    }

    public void OnSpecialAttackClick()
    {
        BlockSpecialPowerButton(true);
        BaseController bc = players[currentTurn.Player];
        Unit unit = currentTurn.Unit;
        bc.OnSpecialPower(unit);
    }

    public void BlockSpecialPowerButton(bool value)
    {
        specialPowerButton.Block(value);
    }

    public void AffectWithSpecialPower(int id, SPECIAL_POWER_TYPE type, BattleAction.UnitSpecialPowerData affectedUnit)
    {
        UnitTurn initiator = GetUnitTurnById(id);
        UnitTurn unitTurn = GetUnitTurnById(affectedUnit.id);
        if (turnList.Contains(unitTurn))
        {
            Unit unit = unitTurn.Unit;
            BaseController bc = players[GetPlayerByUnit(unit)];
            bc.AffectFromSpecialPower(initiator.Unit, unit, type, affectedUnit);
        }
        else
        {
            Debug.LogError("Turn list does'n contain unit with id " + id);
        }
    }

    public void OnSkip()
    {
        BlockSpecialPowerButton(true);
        BlockSkipButton(true);
        OnTurnFinished(currentTurn.Player, currentTurn.UnitId);
    }

    public void BlockSkipButton(bool value)
    {
        skipButton.interactable = !value;
    }

    public void OnBack()
    {
        SoundManager.Instance.PlayOneSound("pausegame");
        Time.timeScale = 0f;
        EventManager.Instance.OnShowDialogWindow(SmallWindow.TITLE.QUIT_GAME, delegate { OnQuit(); }, delegate { OnReturnToGame(); });
    }

    private void OnReturnToGame()
    {
        SoundManager.Instance.PlayOneSound("resumegame");
        EventManager.Instance.OnHideDialogWindow();
        Time.timeScale = 1f;
    }

    public void OnQuit()
    {
        EventManager.Instance.OnHideDialogWindow();
        Time.timeScale = 1f;
        Core.Instance.LoadScene("MainMenu");
    }

    public void ToMap()
    {
        Core.Instance.LoadScene("Map");
    }

    private void EventManager_onSceneLoad()
    {
        levelGUI.SetActive(false);
        winGUI.SetActive(false);
    }
}
