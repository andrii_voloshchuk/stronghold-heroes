﻿using UnityEngine;

public class Map : MonoBehaviour 
{
    public const int GRID_WIDTH = 9;
    public const int GRID_HEIGTH = 6;

    public GameObject gridGameObject;

    public ResourcesInstantiator background;

    public Transform unitsPlacement;
    public Transform movePointer;
    public Transform attackPointer;
    public Transform pointersPlacement;

    private Tile[,] grid;

	public void Init()
	{
        InstantiateBackground();
        InitGrid();
	}

    private void InstantiateBackground()
    {
        string[] prefabs = new string[1];
        string name = "Background_" + Random.Range(1, 9);
        prefabs[0] = name;
        background.ManualInstantiate(null, prefabs);
    }
	
    private void InitGrid()
    {
        grid = new Tile[GRID_WIDTH, GRID_HEIGTH];
        for (int x = 0; x < GRID_WIDTH; x++)
        {
            for (int y = 0; y < GRID_HEIGTH; y++)
            {
                grid[x, y] = new Tile(x, y);
            }
        }
    }

    public Tile GetTile(int x, int y)
    {
        return grid[x, y];
    }

    public Tile GetTile(MapVector position)
    {
        return grid[position.x, position.y];
    }

    public void SetTile(MapVector position, Tile.TileType type)
    {
        grid[position.x, position.y].CurrentTileType = type;
        grid[position.x, position.y].Unit = null;
    }

    public void SetTile(MapVector position, Unit unit, Tile.TileType type)
    {
        grid[position.x, position.y].CurrentTileType = type;
        grid[position.x, position.y].Unit = unit;
    }

    public void ResetPointers()
    {
        movePointer.parent = pointersPlacement;
        movePointer.localPosition = Vector3.zero;
        attackPointer.parent = pointersPlacement;
        attackPointer.localPosition = Vector3.zero;
    }

    public bool CanUnitBeAttacked(Unit attacker, MapVector victimPos)
    {
        Tile victimTile = GetTile(victimPos);
        Tile attackerTile = GetTile(attacker.Position);
        if(victimTile.CurrentTileType == Tile.TileType.UNIT)
        {
            if (victimTile.Unit.IsInvisible)
                return false;

            int minX = Mathf.Max(victimPos.x - 1, 0);
            int maxX = Mathf.Min(victimPos.x + 1, GRID_WIDTH - 1);
            int y = victimPos.y;
            Tile minTile = GetTile(minX, y);
            Tile maxTile =  GetTile(maxX, y);
            if ((minTile.CurrentTileType == Tile.TileType.EMPTY && GameHelper.IsInsideUnitRange(new MapVector(minTile.X, minTile.Y), attacker.Position, attacker))
                || (maxTile.CurrentTileType == Tile.TileType.EMPTY && GameHelper.IsInsideUnitRange(new MapVector(maxTile.X, maxTile.Y), attacker.Position, attacker))
                || minTile == attackerTile || maxTile == attackerTile || (GameHelper.IsUnitArcher(attacker.unitType) && GameHelper.IsInsideUnitRange(victimPos, attacker.Position, attacker)))
            {
                return true;
            }
        }

        return false;
    }
}
