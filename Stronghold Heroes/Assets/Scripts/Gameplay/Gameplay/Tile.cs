﻿public class Tile 
{
    public enum TileType
    {
        EMPTY,
        UNIT
    }

    public int X { get; set; }

    public int Y { get; set; }

    public TileType CurrentTileType { get; set; }

    public Unit Unit { get; set; }

    public Tile(int x, int y)
    {
        CurrentTileType = TileType.EMPTY;
        X = x;
        Y = y;
    }
}
