﻿using System.Collections.Generic;
using UnityEngine;

public class BaseController : MonoBehaviour
{
    public delegate void OnActionCompleted(Unit initiator, Unit victim = null);

    protected Map map;

    protected Dictionary<Unit, UnitController> unitControllers;

    protected Player player;

    protected bool hasTurn;
    protected bool madeAction;
    protected bool usingSpecialPowerWithTarget;

    protected UnitController currentUnit;


    public virtual void Init()
    {
        map = Gameplay.Instance.map;
    }

    protected virtual void SetMovePointer(MapVector position)
    {
        map.ResetPointers();
        map.movePointer.parent = null;
        map.movePointer.transform.localPosition = new Vector3(position.x * 2f + 1f, position.y * 2f + 1f, -10f);
    }

    protected virtual void SetAttackPointer(MapVector position)
    {
        map.ResetPointers();
        map.attackPointer.parent = null;
        map.attackPointer.transform.localPosition = new Vector3(position.x * 2f + 1f, position.y * 2f + 1f, -10f);
    }

    public virtual void InstantiateUnits(Player player, int x)
    {
        this.player = player;
        unitControllers = new Dictionary<Unit, UnitController>();
        int min = (Map.GRID_HEIGTH - player.units.Count) / 2;
        int y = min;
        for (int i = 0; i < player.units.Count; i++)
        {
            Unit unit = player.units[i];
            Vector3 position = new Vector3(x, y, GameHelper.GetUnitPositionZ(y));
            string path = ResourcesManager.UNITS_FOLDER + GameHelper.GetCleanUnitName(unit.unitType);
            UnitController unitController = ResourcesManager.InstantiateGameObject<UnitController>(path, map.unitsPlacement, position);
            unitController.Init(player, unit, GameHelper.DoFlipUnit(x, Map.GRID_WIDTH / 2f));
            unitControllers.Add(unit, unitController);
            map.SetTile(new MapVector(x, y), unit, Tile.TileType.UNIT);
            y++;
        }
    }

    public virtual void TakeTurn(Unit unit)
    {
        if (unitControllers.ContainsKey(unit))
        {
            hasTurn = true;
            currentUnit = unitControllers[unit];
            currentUnit.OnTurnStarted();
            currentUnit.Unit.specialPower.DecrementCoolDown();
            currentUnit.Unit.DecrementEffects();
        }
    }

    public virtual void FinishTurn(Unit unit)
    {
        if (unitControllers.ContainsKey(unit))
        {
            hasTurn = false;
            currentUnit.OnTurnFinished();
            currentUnit = null;
            madeAction = false;
            map.ResetPointers();
        }
    }

    protected virtual void SendMove(MapVector position)
    {
        BattleAction.MoveAction moveAction = new BattleAction.MoveAction(position);
        EventManager.Instance.OnStartBattleAction(currentUnit.Unit.Id, ACTION_TYPE.MOVE, moveAction);
        EventManager.Instance.OnProcessBattleAction();
    }

    protected virtual void SendAttack(MapVector position)
    {
        Tile tile = map.GetTile(position);
        if (currentUnit.mapPosition.y == position.y && (currentUnit.mapPosition.x == position.x - 1 || currentUnit.mapPosition.x == position.x + 1) ||
            GameHelper.IsUnitArcher(currentUnit.Unit.unitType))
        {
            BattleAction.AttackAction attackAction = new BattleAction.AttackAction(new BattleAction.UnitDamageData(tile.Unit.Id, currentUnit.Unit.GetDamage()));
            EventManager.Instance.OnStartBattleAction(currentUnit.Unit.Id, ACTION_TYPE.ATACK, attackAction);
            EventManager.Instance.OnProcessBattleAction();
        }
        else
        {
            int max = Mathf.Min(position.x + 1, Map.GRID_WIDTH - 1);
            int min = Mathf.Max(position.x - 1, 0);
            Tile maxTile = map.GetTile(max, position.y);
            Tile minTile = map.GetTile(min, position.y);
            Tile destination = minTile.CurrentTileType == Tile.TileType.EMPTY ? minTile : maxTile;
            if (currentUnit.mapPosition.x <= min && minTile.CurrentTileType == Tile.TileType.EMPTY)
            {
                destination = minTile;
            }
            else if (currentUnit.mapPosition.x >= max && maxTile.CurrentTileType == Tile.TileType.EMPTY)
            {
                destination = maxTile;
            }
            BattleAction.MoveAction moveAction = new BattleAction.MoveAction(new MapVector(destination.X, destination.Y));
            EventManager.Instance.OnStartBattleAction(currentUnit.Unit.Id, ACTION_TYPE.MOVE, moveAction);
            EventManager.Instance.OnProcessBattleAction();
            BattleAction.AttackAction attackAction = new BattleAction.AttackAction(new BattleAction.UnitDamageData(tile.Unit.Id, currentUnit.Unit.GetDamage()));
            EventManager.Instance.OnStartBattleAction(currentUnit.Unit.Id, ACTION_TYPE.ATACK, attackAction);
        }
    }

    protected virtual void SendSpecialPowerWithTarget(MapVector position)
    {
        Unit target = map.GetTile(position).Unit;
        if (target != null)
        {
            List<Unit> targets = new List<Unit>();
            SPECIAL_POWER_TYPE type = currentUnit.Unit.specialPower.specialPowerType;
            switch (type)
            {
                case SPECIAL_POWER_TYPE.AIMED_SHOT:
                    targets.Add(target);
                    break;
                case SPECIAL_POWER_TYPE.POISON_SHOT:
                    targets.Add(target);
                    break;
            }
            usingSpecialPowerWithTarget = false;
            unitControllers[currentUnit.Unit].UseSpecialPower(position);
            SendSpecialPower(targets);
        }
    }

    protected virtual void SendSpecialPowerWithoutTarget()
    {
        List<Unit> targets = new List<Unit>();
        SPECIAL_POWER_TYPE type = currentUnit.Unit.specialPower.specialPowerType;
        switch (type)
        {
            case SPECIAL_POWER_TYPE.ATTACK_ALL:
            case SPECIAL_POWER_TYPE.TAUNT:
                targets = Gameplay.Instance.GetAllEnemyUnits(player);
                break;
            case SPECIAL_POWER_TYPE.HEAL_ALL:
                targets = Gameplay.Instance.GetAllAllyUnits(player);
                break;
            case SPECIAL_POWER_TYPE.BURNING:
                List<Unit> enemies = Gameplay.Instance.GetAllEnemyUnits(player);
                foreach(Unit enemy in enemies)
                {
                    if(GameHelper.IsInsideRange(currentUnit.Unit, enemy, 2))
                    {
                        targets.Add(enemy);
                    }
                }
                break;
            case SPECIAL_POWER_TYPE.SHIELD:
            case SPECIAL_POWER_TYPE.INCREASED_DAMAGE:
            case SPECIAL_POWER_TYPE.INVISIBILITY:
            case SPECIAL_POWER_TYPE.RAGE:
                targets.Add(currentUnit.Unit);
                break;
        }
        SendSpecialPower(targets);
    }

    private void SendSpecialPower(List<Unit> targets)
    {
        currentUnit.Unit.specialPower.SetCoolDown();
        if (targets.Count > 0)
        {
            foreach (Unit target in targets)
            {
                Unit unit = currentUnit.Unit;
                BattleAction.SpecialPowerAction specialPowerAction =
                    new BattleAction.SpecialPowerAction(unit.specialPower.specialPowerType, unit.GetSpecialPowersEffects(target));
                EventManager.Instance.OnStartBattleAction(currentUnit.Unit.Id, ACTION_TYPE.SPECIAL_POWER, specialPowerAction);
            }
            EventManager.Instance.OnProcessBattleAction();
        }
        else
        {
            Gameplay.Instance.OnSkip();
        }
    }

    public virtual void MoveUnit(Unit unit, MapVector position)
    {
        if (unitControllers.ContainsKey(unit))
        {
            unitControllers[unit].Move(position, OnMoveCompleted);
        }
        else
        {
            Debug.LogError("Player " + player.name + " doesn't have unit " + unit.unitType);
        }
    }

    private void OnMoveCompleted(Unit unit, Unit victim = null)
    {
        EventManager.Instance.OnFinishBattleAction(unit.Id, ACTION_TYPE.MOVE);
    }

    public virtual void AttackUnit(Unit unit, Unit victim)
    {
        if (unitControllers.ContainsKey(unit))
        {
            unitControllers[unit].Attack(victim);
        }
        else
        {
            Debug.LogError("Player " + player.name + " doesn't have unit " + unit.unitType);
        }
    }

    public virtual void DamageUnit(Unit attacker, Unit victim, float damage)
    {
        if (unitControllers.ContainsKey(victim))
        {
            StartCoroutine(unitControllers[victim].Damage(GameHelper.GetDamageDelay(attacker, victim.Position), attacker, damage, OnDamageCompleted));
        }
        else
        {
            Debug.LogError("Player " + player.name + " doesn't have unit " + victim.unitType);
        }
    }

    private void OnDamageCompleted(Unit attacker, Unit victim)
    {
        if (victim.IsDead)
        {
            BattleAction.DeathAction deathAction = new BattleAction.DeathAction(victim.Id);
            EventManager.Instance.OnStartBattleAction(attacker.Id, ACTION_TYPE.DEATH, deathAction);
        }
        EventManager.Instance.OnFinishBattleAction(attacker.Id, ACTION_TYPE.ATACK);

    }

    public virtual void KillUnit(Unit unit, Unit killer)
    {
        if (unitControllers.ContainsKey(unit))
        {
            unitControllers[unit].Die(killer, OnDieCompleted);
        }
        else
        {
            Debug.LogError("Player " + player.name + " doesn't have unit " + unit.unitType);
        }
    }

    private void OnDieCompleted(Unit unit, Unit victim = null)
    {
        EventManager.Instance.OnFinishBattleAction(unit.Id, ACTION_TYPE.DEATH);
    }

    public virtual void AffectFromSpecialPower(Unit initiator, Unit unit, SPECIAL_POWER_TYPE type, BattleAction.UnitSpecialPowerData affectedUnit)
    {
        if (unitControllers.ContainsKey(unit))
        {
            unitControllers[unit].AffectFromSpecialPower(initiator, type, affectedUnit, OnAffectComleted);
        }
        else
        {
            Debug.LogError("Player " + player.name + " doesn't have unit " + unit.unitType);
        }
    }

    private void OnAffectComleted(Unit inititator, Unit victim = null)
    {
        if (victim != null && victim.IsDead)
        {
            BattleAction.DeathAction deathAction = new BattleAction.DeathAction(victim.Id);
            EventManager.Instance.OnStartBattleAction(inititator.Id, ACTION_TYPE.DEATH, deathAction);
        }
        EventManager.Instance.OnFinishBattleAction(inititator.Id, ACTION_TYPE.SPECIAL_POWER);
    }

    public virtual void OnSpecialPower(Unit unit)
    {
       
    }
}
