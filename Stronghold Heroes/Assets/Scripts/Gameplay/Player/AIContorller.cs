﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AIContorller : BaseController 
{
    private Algorithm algorithm;

    public override void Init()
    {
        base.Init();
        algorithm = new Algorithm();
    }

    private ALLIES_ALIVE GetAliveAlliesCount()
    {
        ALLIES_ALIVE result = ALLIES_ALIVE.NONE;
        List<Unit> allies = Gameplay.Instance.GetAllAllyUnits(player);
        int count = -1;
        int totalCount = -1;
        foreach(Unit unit in allies)
        {
            if (!unit.IsDead)
                count++;
            totalCount++;
        }
        if(count > 0 && count < totalCount / 2)
        {
            result = ALLIES_ALIVE.FEW;
        }
        else if (count > 0 && count >= totalCount / 2)
        {
            result = ALLIES_ALIVE.MANY;
        }

        return result;
    }

    private UNIT_CLASS GetUnitClass(Unit unit)
    {
        return unit.specialPower.needsTarget ? UNIT_CLASS.ATTACKER : UNIT_CLASS.HEALER;
    }

    private HP_LEVEL GetHpLevel(Unit unit)
    {
        HP_LEVEL result = HP_LEVEL.HIGH;
        if(unit.CurrentHp <= unit.hp / 3)
        {
            result = HP_LEVEL.LOW;
        }
        else if(unit.CurrentHp > unit.hp / 3 && unit.CurrentHp <= 2 * (unit.hp / 3))
        {
            result = HP_LEVEL.MEDIUM;
        }

        return result;
    }

    private SPECIAL_POWER_AVAILABLE GetSpecialPowerAvailable(Unit unit)
    {
        return unit.specialPower.GetCurrentCoolDown() > 0 ? SPECIAL_POWER_AVAILABLE.NO : SPECIAL_POWER_AVAILABLE.YES;
    }

    private ENEMIES_IN_RANGE GetEnemiesInRangeCount(Unit unit)
    {
        ENEMIES_IN_RANGE result = ENEMIES_IN_RANGE.NONE;
        List<Unit> enemies = Gameplay.Instance.GetAllEnemyUnits(player);
        int count = 0;
        int totalCount = 0;
        foreach(Unit enemy in enemies)
        {
            if(!enemy.IsDead && GameHelper.IsInsideUnitRange(enemy.Position, unit.Position, unit) && map.CanUnitBeAttacked(unit, enemy.Position))
            {
                count++;
            }
            totalCount++;
        }

        if(count > 0 && count >= totalCount / 2)
        {
            result = ENEMIES_IN_RANGE.MANY;
        }
        else if(count > 0 && count < totalCount / 2)
        {
            result = ENEMIES_IN_RANGE.FEW;
        }

        return result;
    }

    private List<Unit> GetAllEnemiesInRange(Unit unit)
    {
        List<Unit> allEnemies = Gameplay.Instance.GetAllEnemyUnits(player);
        List<Unit> enemies = new List<Unit>();
        foreach (Unit enemy in allEnemies)
        {
            if (!enemy.IsDead && GameHelper.IsInsideUnitRange(enemy.Position, unit.Position, unit) && map.CanUnitBeAttacked(unit, enemy.Position))
            {
                enemies.Add(enemy);
            }
        }
        return enemies;
    }

    private Unit GetStrongestEnemyInRange(Unit unit)
    {
        return GetAllEnemiesInRange(unit).OrderByDescending(i => i.CurrentHp).ThenByDescending(i => i.damage).ThenByDescending(i => i.GetAllPositiveEffectsCount()).FirstOrDefault();
    }

    private Unit GetWeakestEnemyInRange(Unit unit)
    {
        return GetAllEnemiesInRange(unit).OrderBy(i => i.CurrentHp).ThenBy(i => i.damage).ThenBy(i => i.GetAllPositiveEffectsCount()).FirstOrDefault();
    }

    private Unit GetStrongestEnemy()
    {
        List<Unit> allEnemies = Gameplay.Instance.GetAllEnemyUnits(player);
        return allEnemies.OrderByDescending(i => i.CurrentHp).ThenByDescending(i => i.damage).ThenByDescending(i => i.GetAllPositiveEffectsCount()).FirstOrDefault();
    }

    private Unit GetWeakestEnemy()
    {
        List<Unit> allEnemies = Gameplay.Instance.GetAllEnemyUnits(player);
        return allEnemies.OrderBy(i => i.CurrentHp).ThenBy(i => i.damage).ThenBy(i => i.GetAllPositiveEffectsCount()).FirstOrDefault();
    }

    private void Escape(Unit unit)
    {
        GameHelper.UnitRange range = GameHelper.GetUnitRange(unit.Position.x, unit.Position.y, unit);
        int leftDistnce = Mathf.Abs(unit.Position.x - range.minX);
        int rightDistance = Mathf.Abs(range.maxX - unit.Position.x);
        int furthestCorner = Mathf.Max(leftDistnce, rightDistance) == leftDistnce ? range.minX : range.maxX;
        List<Tile> availableTiles = GetAvailableTiles(unit);
        Tile destinatioTile = availableTiles.OrderBy(i => (Vector2.Distance(new Vector2(i.X, i.Y), new Vector2(furthestCorner, i.Y)))).FirstOrDefault();
        SendMove(new MapVector(destinatioTile.X, destinatioTile.Y));
    }

    private List<Tile> GetAvailableTiles(Unit unit)
    {
        GameHelper.UnitRange moveRange = GameHelper.GetUnitRange(unit.Position.x, unit.Position.y, unit);

        List<Tile> availableTiles = new List<Tile>();
        for (int x = moveRange.minX; x < moveRange.maxX; x++)
        {
            for (int y = moveRange.minY; y < moveRange.maxY; y++)
            {
                Tile tile = map.GetTile(x, y);
                if (tile.CurrentTileType == Tile.TileType.EMPTY)
                {
                    availableTiles.Add(tile);
                }
            }
        }

        return availableTiles;
    }

    private void MoveToEnemy(Unit unit, Unit enemy)
    {
        List<Tile> availableTiles = GetAvailableTiles(unit);
        Tile destinatioTile = availableTiles.OrderBy(i => (Vector2.Distance(new Vector2(i.X, i.Y), new Vector2(enemy.Position.x, enemy.Position.y)))).FirstOrDefault();
        if (destinatioTile == null)
        {
            Gameplay.Instance.OnSkip();
        }
        else
        {
            SendMove(new MapVector(destinatioTile.X, destinatioTile.Y));
        }
    }

    private IEnumerator MakeDecision(float delay)
    {
        yield return new WaitForSeconds(1f);
        Unit unit = currentUnit.Unit;
        Rule rule = new Rule();
        rule.alliesAlive = GetAliveAlliesCount();
        rule.enemiesInRange = GetEnemiesInRangeCount(unit);
        rule.hpLevel = GetHpLevel(unit);
        rule.specialPowerAvailable = GetSpecialPowerAvailable(unit);
        rule.unitClass = GetUnitClass(unit);

        DECISION decision = algorithm.GetDecision(rule);
        Log.AI(decision);

        switch(decision)
        {
            case DECISION.MOVE_TO_WEAKEST:
                MoveToEnemy(unit, GetWeakestEnemy());
                break;
            case DECISION.MOVE_TO_STRONGEST:
                MoveToEnemy(unit, GetStrongestEnemy());
                break;
            case DECISION.ATTACK_WEAKEST:
                SendAttack(GetWeakestEnemyInRange(unit).Position);
                break;
            case DECISION.ATTACK_STRONGEST:
                SendAttack(GetStrongestEnemyInRange(unit).Position);
                break;
            case DECISION.USE_SPECIAL_POWER:
                OnSpecialPower(currentUnit.Unit);
                SendSpecialPowerWithoutTarget();
                break;
            case DECISION.USE_SPECIAL_POWER_ON_WEAKEST:
                OnSpecialPower(currentUnit.Unit);
                SendSpecialPowerWithTarget(GetWeakestEnemyInRange(unit).Position);
                break;
            case DECISION.USE_SPECIAL_POWER_ON_STRONGEST:
                OnSpecialPower(currentUnit.Unit);
                SendSpecialPowerWithTarget(GetStrongestEnemyInRange(unit).Position);
                break;
            case DECISION.ESCAPE:
                Escape(unit);
                break;
        }
    }

    public override void InstantiateUnits(Player player, int x)
    {
        base.InstantiateUnits(player, x);
    }

    public override void TakeTurn(Unit unit)
    {
        if (unitControllers.ContainsKey(unit))
        {
            base.TakeTurn(unit);
            Gameplay.Instance.BlockSpecialPowerButton(true);
            Gameplay.Instance.BlockSkipButton(true);
            StartCoroutine(MakeDecision(1f));
        }
    }

    public override void FinishTurn(Unit unit)
    {
        if (unitControllers.ContainsKey(unit))
        {
            base.FinishTurn(unit);
            Gameplay.Instance.BlockSpecialPowerButton(false);
            Gameplay.Instance.BlockSkipButton(true);
        }
    }

    protected override void SendMove(MapVector position)
    {
        base.SendMove(position);
    }

    protected override void SendAttack(MapVector position)
    {
        base.SendAttack(position);
    }

    protected override void SendSpecialPowerWithTarget(MapVector position)
    {
        base.SendSpecialPowerWithTarget(position);
    }

    protected override void SendSpecialPowerWithoutTarget()
    {
        base.SendSpecialPowerWithoutTarget();
    }

    public override void MoveUnit(Unit unit, MapVector position)
    {
        base.MoveUnit(unit, position);
    }

    public override void AttackUnit(Unit unit, Unit victim)
    {
        base.AttackUnit(unit, victim);
    }

    public override void DamageUnit(Unit attacker, Unit victim, float damage)
    {
        base.DamageUnit(attacker, victim, damage);
    }

    public override void KillUnit(Unit unit, Unit killer)
    {
        base.KillUnit(unit, killer);
    }

    public override void AffectFromSpecialPower(Unit initiator, Unit unit, SPECIAL_POWER_TYPE type, BattleAction.UnitSpecialPowerData affectedUnit)
    {
        base.AffectFromSpecialPower(initiator, unit, type, affectedUnit);
    }

    public override void OnSpecialPower(Unit unit)
    {
        if (unitControllers.ContainsKey(unit))
        {
            if (unit.specialPower.needsTarget)
            {
                usingSpecialPowerWithTarget = true;
                map.ResetPointers();
            }
            else
            {
                unitControllers[unit].UseSpecialPower(unit.Position);
            }
        }
        else
        {
            Debug.LogError("Player " + player.name + " doesn't have unit " + unit.unitType);
        }
    }

}
