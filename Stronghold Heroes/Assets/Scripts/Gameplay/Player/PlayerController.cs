﻿using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(BoxCollider2D))]
public class PlayerController : BaseController 
{
    private class Intent
    {
 
        public INTENT_TYPE type;
        public MapVector position;

        public Intent(INTENT_TYPE type, MapVector position)
        {
            this.type = type;
            this.position = position;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (obj.GetType() != GetType())
                return false;
            Intent other = (Intent)obj;
            return position.Equals(other.position) && type == other.type;
        }

        public override int GetHashCode()
        {
            return (int)type * position.x + (int)type * position.x * position.y;
        }
    }   


    private BoxCollider2D boxCollider2d;

    private Intent currentIntent;

    private void Awake()
    {
        boxCollider2d = GetComponent<BoxCollider2D>();
        boxCollider2d.enabled = false;
    }

    public override void Init()
    {
        base.Init();
    }

    private void OnMouseDown()
    {
        if (hasTurn && !madeAction && !EventSystem.current.IsPointerOverGameObject())
        {
            Vector3 ray = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            MapVector position = GameHelper.GetPositionOnGrid(ray);
            if (currentIntent == null)
            {
                currentIntent = GetIntent(position);
            }
            else
            {
                Intent intent = GetIntent(position);
                if (currentIntent.Equals(intent))
                {
                    madeAction = true;
                    Gameplay.Instance.BlockSkipButton(true);
                    switch (currentIntent.type)
                    {
                        case INTENT_TYPE.MOVE:
                            SendMove(position);
                            break;
                        case INTENT_TYPE.ATTACK:
                            SendAttack(position);
                            break;
                        case INTENT_TYPE.SPECIAL_POWER:
                            if (usingSpecialPowerWithTarget)
                            {
                                SendSpecialPowerWithTarget(position);
                            }
                            else
                            {
                                SendSpecialPowerWithTarget(position);
                            }
                            break;
                    }
                    map.ResetPointers();
                }
                else
                {
                    currentIntent = GetIntent(position);
                }
            }
        }
    }

    private Intent GetIntent(MapVector position)
    {
        Intent intent = null;
        Tile tile = map.GetTile(position);

        switch (tile.CurrentTileType)
        {
            case Tile.TileType.EMPTY:
                if (GameHelper.IsInsideUnitRange(position, currentUnit.mapPosition, currentUnit.Unit) && !usingSpecialPowerWithTarget)
                {
                    intent = new Intent(INTENT_TYPE.MOVE, position);
                    SetMovePointer(position);
                }
            break;
            case Tile.TileType.UNIT:
                UNIT_TEAM team = tile.Unit.UnitTeam;
                if(team != player.unitTeam)
                {
                    if (GameHelper.IsInsideUnitRange(position, currentUnit.mapPosition, currentUnit.Unit) && map.CanUnitBeAttacked(currentUnit.Unit, position))
                    {
                        if (usingSpecialPowerWithTarget)
                        {
                            intent = new Intent(INTENT_TYPE.SPECIAL_POWER, position);
                        }
                        else
                        {
                            intent = new Intent(INTENT_TYPE.ATTACK, position);
                        }
                        SetAttackPointer(position);
                    }
                }
            break;
        }
        if(intent == null)
        {
            map.ResetPointers();
        }
        return intent;
    }

    public override void InstantiateUnits(Player player, int x)
    {
        base.InstantiateUnits(player, x);
    }

    public override void TakeTurn(Unit unit)
    {
        if (unitControllers.ContainsKey(unit))
        {
            base.TakeTurn(unit);
            boxCollider2d.enabled = true;
            Gameplay.Instance.BlockSkipButton(false);
        }
    }

    public override void FinishTurn(Unit unit)
    {
        if (unitControllers.ContainsKey(unit))
        {
            base.FinishTurn(unit);
            boxCollider2d.enabled = false;
            currentIntent = null;
        }
    }

    protected override void SendMove(MapVector position)
    {
        base.SendMove(position);
    }

    protected override void SendAttack(MapVector position)
    {
        base.SendAttack(position);
    }

    protected override void SendSpecialPowerWithTarget(MapVector position)
    {
        base.SendSpecialPowerWithTarget(position);
    }

    protected override void SendSpecialPowerWithoutTarget()
    {
        base.SendSpecialPowerWithoutTarget();
    }

    public override void MoveUnit(Unit unit, MapVector position)
    {
        base.MoveUnit(unit, position);
    }

    public override void AttackUnit(Unit unit, Unit victim)
    {
        base.AttackUnit(unit, victim);
    }

    public override void DamageUnit(Unit attacker, Unit victim, float damage)
    {
        base.DamageUnit(attacker, victim, damage);
    }

    public override void KillUnit(Unit unit, Unit killer)
    {
        base.KillUnit(unit, killer);
    }

    public override void AffectFromSpecialPower(Unit initiator, Unit unit, SPECIAL_POWER_TYPE type, BattleAction.UnitSpecialPowerData affectedUnit)
    {
        base.AffectFromSpecialPower(initiator, unit, type, affectedUnit);
    }

    public override void OnSpecialPower(Unit unit)
    {
        if (unitControllers.ContainsKey(unit))
        {
            if (unit.specialPower.needsTarget)
            {
                usingSpecialPowerWithTarget = true;
                map.ResetPointers();
            }
            else
            {
                if (!madeAction)
                {
                    madeAction = true;
                    unitControllers[unit].UseSpecialPower(unit.Position);
                    SendSpecialPowerWithoutTarget();
                }
            }
        }
        else
        {
            Debug.LogError("Player " + player.name + " doesn't have unit " + unit.unitType);
        }
    }
}
