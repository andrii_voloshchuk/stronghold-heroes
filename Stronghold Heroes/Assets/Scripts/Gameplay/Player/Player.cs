﻿using System.Collections.Generic;
using System;

[Serializable]
public class Player  
{
    public string name;

    public AVATAR avatar;

    public List<Unit> units;

    [NonSerialized]
    public UNIT_TEAM unitTeam;

    [NonSerialized]
    public bool isAi; 

    public Player (Player other)
    {
        units = new List<Unit>();
        name = other.name;
        avatar = other.avatar;
        foreach(Unit unit in other.units)
        {
            units.Add(new Unit(unit));
        }
    }

    public Player(string name)
    {
        this.name = name;
        units = new List<Unit>();
    }
}
