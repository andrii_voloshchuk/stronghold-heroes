﻿using UnityEngine;

public class Arrow : MonoBehaviour 
{
    private Vector3 destination;

    private bool move;

    public float speed;

	public void SetDestination(MapVector position)
    {
        destination = new Vector3(position.x * 2f + 1, position.y * 2f + 1, GameHelper.GetUnitPositionZ(position.y) * 2f);
        move = true;
    }
	
	private void Update() 
	{
	    if(move)
        {
            Vector3 diff = destination - transform.position;
            diff.Normalize();
            float z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, z);

            transform.localPosition = Vector3.MoveTowards(transform.localPosition, destination, Time.deltaTime * speed);
            if(transform.localPosition == destination)
            {
                Destroy(gameObject);
            }
        }
	}
}
