﻿using UnityEngine;

public class HealthBar : MonoBehaviour 
{
    public SpriteRenderer bar;

    public void SetHealth(float percent)
    {
        if (percent < 0f)
            percent = 0f;
        float size = percent / 100f;
        bar.transform.localScale = new Vector3(size, 1f, 1f);
        Color color = percent <= 50f ? (percent <= 20f ? Color.red : Color.yellow) : Color.green;
        bar.color = color;
    }
	
}
