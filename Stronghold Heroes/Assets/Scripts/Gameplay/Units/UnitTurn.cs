﻿public class UnitTurn  
{
    public Unit Unit { get; private set; }

    public Player Player { get; private set; }

    public UNIT_TEAM UnitTeam { get; private set; }

    public int UnitId { get; private set; }

    public UnitTurn(Player player, Unit unit, UNIT_TEAM unitTeam)
    {
        UnitId = unit.Id;
        Player = player;
        Unit = unit;
        UnitTeam = unitTeam;
    }
}
