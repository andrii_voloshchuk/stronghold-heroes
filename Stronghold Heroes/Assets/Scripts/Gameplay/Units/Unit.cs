﻿using System;
using System.Collections.Generic;

[Serializable]
public class Unit : IComparable
{
    private class Effect
    {
        public SPECIAL_POWER_TYPE type;
        public int duration;
        public bool positive;

        public Effect(SPECIAL_POWER_TYPE type, int duration, bool positive)
        {
            this.type = type;
            this.duration = duration;
            this.positive = positive;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (obj.GetType() != this.GetType())
                return false;
            Effect other = (Effect)obj;
            return type == other.type;
        }

        public override int GetHashCode()
        {
            return (int)type * (duration + (positive ? 1 : 0)) * duration; 
        }
    }

    public UNIT_TYPE unitType;

    public int level;
    public float hp;
    public float damage;

    public int range;

    public SpecialPower specialPower;

    public UNIT_TEAM UnitTeam { get; set; }

    public int Id { get; set; }

    public float CurrentHp { get; private set; }

    public bool IsDead { get; private set; }

    public MapVector Position { get; private set; }

    public float Shield { get; private set; }

    public float IncreasedDamage { get; private set; }

    public float Poison { get; private set; }

    public bool IsInvisible { get; private set; }

    private List<Effect> effects;

    public IUnitBinder iUnitBinder;

    public Unit()
    {
        effects = new List<Effect>();
    }

    public Unit(Unit other)
    {
        effects = new List<Effect>();
        hp = other.hp;
        level = other.level;
        range = other.range;
        specialPower = new SpecialPower(other.specialPower);
        unitType = other.unitType;
        damage = other.damage;
        CurrentHp = hp;
    }

    public void SetPosition(MapVector position)
    {
        Position = position;
    }

    public void Damage(float damage)
    {
        float shield = Shield > 0 ? Shield : 1f;
        CurrentHp -= damage * shield;
        if(CurrentHp <= 0)
        {
            IsDead = true;
        }
    }
	
    public float Heal(float percent)
    {
        float heal = hp * percent;
        CurrentHp += heal;
        if(CurrentHp > hp)
        {
            CurrentHp = hp;
        }
        return heal;
    }

    public void SetShield(float shield, int duration)
    {
        Shield = shield;
        AddEffect(new Effect(SPECIAL_POWER_TYPE.SHIELD, duration, true));
    }

    public void SetIncreasedDamage(float damage, int duration)
    {
        IncreasedDamage += damage;
        AddEffect(new Effect(SPECIAL_POWER_TYPE.INCREASED_DAMAGE, duration, true));
    }

    public void SetTaunt(float damage, int duration)
    {
        IncreasedDamage -= damage;
        AddEffect(new Effect(SPECIAL_POWER_TYPE.TAUNT, duration, false));
    }

    public void SetPoison(float poison, int duration)
    {
        Poison += poison;
        AddEffect(new Effect(SPECIAL_POWER_TYPE.POISON_SHOT, duration, false));
    }

    public void SetBurning(float burning, int duration)
    {
        Poison += burning;
        AddEffect(new Effect(SPECIAL_POWER_TYPE.BURNING, duration, false));
    }

    public void SetInvisibility(int duration)
    {
        IsInvisible = true;
        AddEffect(new Effect(SPECIAL_POWER_TYPE.INVISIBILITY, duration, true));
    }

    public float GetDamage()
    {
        float dam = GameHelper.GetRandomUnitDamage(damage);
        float increased = dam + (dam * IncreasedDamage);
        if (increased < 0)
            increased = 0;
        return increased;
    }

    private void AddEffect(Effect effect)
    {
        if(!effects.Contains(effect))
        {
            effects.Add(effect);
        }
    }

    public void DecrementEffects()
    {
        for (int i = effects.Count - 1; i >= 0; i--)
        {
            effects[i].duration--;
            if (effects[i].duration <= 0)
            {
                RemoveEffect(effects[i].type);
                effects.RemoveAt(i);
            }
        }
    }

    private void RemoveEffect(SPECIAL_POWER_TYPE type)
    {
        switch(type)
        {
            case SPECIAL_POWER_TYPE.SHIELD:
                Shield = 0f;
                break;
            case SPECIAL_POWER_TYPE.INCREASED_DAMAGE:
                IncreasedDamage = 0f;
                break;
            case SPECIAL_POWER_TYPE.POISON_SHOT:
            case SPECIAL_POWER_TYPE.BURNING:
                Poison = 0f;
                break;
            case SPECIAL_POWER_TYPE.INVISIBILITY:
                IsInvisible = false;
                break;
        }
        iUnitBinder.OnRemoveEffect(type);
    }

    public void RemoveAllPositiveEffects()
    {
        for (int i = effects.Count - 1; i >= 0; i--)
        {
            if (effects[i].positive)
            {
                RemoveEffect(effects[i].type);
                effects.RemoveAt(i);
            }
        }
    }

    public int GetAllPositiveEffectsCount()
    {
        int count = 0;
        for (int i = effects.Count - 1; i >= 0; i--)
        {
            if (effects[i].positive)
            {
                count++;
            }
        }
        return count;
    }

    public BattleAction.UnitSpecialPowerData GetSpecialPowersEffects(Unit target)
    {
        BattleAction.UnitSpecialPowerData unit = new BattleAction.UnitSpecialPowerData(target.Id, specialPower.duration,
                GameHelper.GetRandomUnitSpecialPowerEffect(specialPower.primaryParameter), GameHelper.GetRandomUnitSpecialPowerEffect(specialPower.secondaryParameter));
        
        return unit;
    }

    public int CompareTo(object obj)
    {
        if (obj == null)
            return 1;
        if (obj.GetType() != GetType())
            return 1;

        Unit other = (Unit)obj;

        if (level > other.level)
            return -1;
        else
            return 1;
    }
}
