﻿using System;

[Serializable]
public class SpecialPower 
{
    public SPECIAL_POWER_TYPE specialPowerType;

    public int coolDown;
    public int duration;

    public float primaryParameter;
    public float secondaryParameter;

    public bool needsTarget;

    private int currentCooldDown;

    public SpecialPower(SpecialPower other)
    {
        specialPowerType = other.specialPowerType;
        coolDown = other.coolDown;
        currentCooldDown = coolDown;
        duration = other.duration;
        primaryParameter = other.primaryParameter;
        secondaryParameter = other.secondaryParameter;
        needsTarget = other.needsTarget;
    }

    public int GetCurrentCoolDown()
    {
        return coolDown - currentCooldDown; 
    }

    public void SetCoolDown()
    {
        currentCooldDown = 0; 
    }

    public void DecrementCoolDown()
    {
        currentCooldDown++;
        if (currentCooldDown > coolDown)
            currentCooldDown = coolDown;
    }
}
