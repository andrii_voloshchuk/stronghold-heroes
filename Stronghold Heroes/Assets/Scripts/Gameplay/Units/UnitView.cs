﻿using UnityEngine;

public class UnitView : MonoBehaviour 
{
    private const string FIRE_PATH = "Gameplay/Map/Fire";

    public enum State
    {
        Start,
        Idle,
        Move,
        Attack,
        Special,
        Die_Arrow,
        Die_Sword
    }

    public GameObject[] views;

    private Animator animator;
    private SpriteRenderer spriteRenderer;

    private bool isInitialized;
    private bool isDead;
    public bool isFlipped;

    private State currentState;

    private float idleTimeDelta;
    private float idleTime;

    private Animator fire;

    public void Init(UNIT_TEAM unitTeam, bool flip = false)
    {
        int index = (int)unitTeam;
        if(index < views.Length)
        {
            isFlipped = flip;
            views[index].SetActive(true);
            animator = views[index].GetComponent<Animator>();
            spriteRenderer = views[index].GetComponent<SpriteRenderer>();
            Flip(flip);
            idleTime = GetRandomIdleTime(1f, 10f);
            isInitialized = true;
        }
        else
        {
            Debug.LogError("Cannot find view for team " + unitTeam.ToString() + " in " + name);
        }
    }
	
    public void SetState(State state)
    {
        idleTimeDelta = 0f;
        currentState = state;
        animator.SetTrigger(state.ToString());
        idleTime = GetRandomIdleTime(5f, 30f);
        CheckIsDead(state);
    }
    
    private void CheckIsDead(State state)
    {
        if (state == State.Die_Sword || state == State.Die_Arrow)
        {
            isDead = true;
        }
    }

    private float GetRandomIdleTime(float min, float max)
    {
        return Random.Range(min, max);
    }
    
    public void Flip(bool flip)
    {
        isFlipped = flip;
        spriteRenderer.flipX = flip;
    }

    public void SetInvisible(bool invisible)
    {
        Color color = invisible ? new Color(1f, 1f, 1f, 0.4f) : new Color(1f, 1f, 1f, 1f);
        spriteRenderer.material.SetColor("_Color", color);
    }

    public void AddFire()
    {
        if(fire == null)
        {
            fire = ResourcesManager.InstantiateGameObject<Animator>(FIRE_PATH, transform, new Vector3(0f, -0.5f, -0.1f));
        }
    }

    public void RemoveFire()
    {
        if(fire != null)
        {
            fire.SetTrigger("Finish");
            Destroy(fire.gameObject, 1f);
            fire = null;
        }
    }

    private void Update()
    {
        if(isInitialized && !isDead)
        {
            idleTimeDelta += Time.deltaTime;
            if(idleTimeDelta >= idleTime)
            {
                idleTimeDelta = 0f;
                if(currentState.Equals(State.Start))
                {
                    SetState(State.Idle);
                    idleTime = GetRandomIdleTime(2f, 5f);
                }
                else if(currentState.Equals(State.Idle))
                {
                    SetState(State.Start);
                    idleTime = GetRandomIdleTime(5f, 30f);
                }
            }
        }
    }

}
