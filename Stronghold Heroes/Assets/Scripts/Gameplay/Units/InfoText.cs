﻿using UnityEngine;

[RequireComponent(typeof(TextMesh))]
public class InfoText : MonoBehaviour 
{
    private TextMesh textMesh;

    private bool move;

    public float speed;

	private void Awake()
	{
        textMesh = GetComponent<TextMesh>();
	}

    public void SetText(string text, Color color, float delay = 1f)
    {
        textMesh.text = text;
        textMesh.color = color;
        Destroy(gameObject, delay);
        move = true;
    }

    private void Update() 
	{
	    if(move)
        {
            transform.Translate(Vector3.up * Time.deltaTime * speed);
        }
	}
}
