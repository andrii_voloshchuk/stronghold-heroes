﻿using UnityEngine;
using System.Collections.Generic;

public class EffectsBar : MonoBehaviour 
{
    private const string EFFECTS_PATH = "GamePlay/Effects/";

    private Dictionary<SPECIAL_POWER_TYPE, Transform> effects;

    private void Awake()
    {
        effects = new Dictionary<SPECIAL_POWER_TYPE, Transform>();
    }
	
    public void AddEffect(SPECIAL_POWER_TYPE type)
    {
        if(!effects.ContainsKey(type))
        {
            string path = EFFECTS_PATH + type;
            Transform t = ResourcesManager.InstantiateGameObject(path, transform, Vector3.zero).transform;
            effects.Add(type, t);
            ReorderEffects();
        }
    }

    private void ReorderEffects()
    {
        float distance = 0.4f;
        float step = distance / 2f;
        float start = -step * (effects.Count - 1);
        
        foreach(var entry in effects)
        {
            entry.Value.localPosition = new Vector3(start, 0, 0);
            start += distance;
        }
    }

    public void RemoveEffect(SPECIAL_POWER_TYPE type)
    {
        if (effects.ContainsKey(type))
        {
            Destroy(effects[type].gameObject);
            effects.Remove(type);
            ReorderEffects();
        }
    }
}
