﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

public class UnitController : MonoBehaviour, IUnitBinder
{
    private const string TILE_PATH = "Gameplay/Map/Tile";
    private const string ARROW_PATH = "Gameplay/Map/Arrow";
    private const string TEXT_PATH = "Gameplay/Map/Text";
    private const string UNTIS_PATH = "Units/";
    private const string ATTACK_PATH = "Attack/";
    private const string MOVE_PATH = "Move/";
    private const string SPECIAL_PATH = "Special/";
    private const string START_PATH = "Start/";
    private const string OTHER_PATH = "Other/";
    private const string HIT_PATH = "Hit/";
    private const string DEATH_ARROW_PATH = "Arrow/";
    private const string DEATH_SWORD_PATH = "Sword/";

    private class UnitTile
    {
        public Vector2 position;
        public SpriteRenderer spriteRenderer;

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (obj.GetType() != GetType()) 
                return false;
            UnitTile other = (UnitTile)obj;
            return position.x == other.position.x && position.y == other.position.y;
        }

        public override int GetHashCode()
        {
            return (int)position.x * ((int)position.y + (int)position.x);
        }

        public UnitTile(Vector2 position, SpriteRenderer spriteRenderer)
        {
            this.position = position;
            this.spriteRenderer = spriteRenderer;
        }
    }

    [Serializable]
    public struct Sounds
    {
        public int attackCount;
        public int moveCount;
        public int startCount;

        public bool hasSoundOnSpecial;
    }

    private Unit target;
    private Unit unit;
    public Unit Unit { get { return unit; } }

    public UnitView unitView;

    public HealthBar healthBar;

    public EffectsBar effectsBar;

    public MapVector mapPosition;

    public Transform tilesPlacement;
 
    private List<UnitTile> tiles;

    public GameObject chosen;

    public float speed;

    [Space(8)]
    [Header("Sounds")]
    public Sounds sounds;

    private bool startMoving;

    private Vector3 destination;

    private Map map;

    private BaseController.OnActionCompleted onMoveActionCompleted;

    private Player player;

	public void Init(Player player, Unit unit, bool flip)
    {
        this.player = player;
        map = Gameplay.Instance.map;
        this.unit = unit;
        unit.iUnitBinder = this;
        RefreshPosition();
        unitView.Init(unit.UnitTeam, flip);
    }

    public void Move(MapVector position, BaseController.OnActionCompleted onActionCompleted)
    {
        SoundManager.Instance.PlayRandomSoundFromResources(UNTIS_PATH + GameHelper.GetCleanUnitName(unit.unitType) + "/" + MOVE_PATH, 1, sounds.moveCount);
        onMoveActionCompleted = onActionCompleted;
        map.SetTile(mapPosition, Tile.TileType.EMPTY);
        DestroyTiles();
        destination = new Vector3(position.x, position.y, GameHelper.GetUnitPositionZ(position.y));
        unitView.Flip(GameHelper.DoFlipUnit(transform.localPosition.x, destination.x));
        unitView.SetState(UnitView.State.Move);
        startMoving = true;
    }

    private void OnMoveCompleted()
    {
        startMoving = false;
        RefreshPosition();
        map.SetTile(mapPosition, unit, Tile.TileType.UNIT);
        destination = transform.position;
        unitView.SetState(UnitView.State.Start);
        CheckFlip();
        onMoveActionCompleted(unit);
        onMoveActionCompleted = null;
    }

    public void Attack(Unit victim)
    {
        SoundManager.Instance.PlayRandomSoundFromResources(UNTIS_PATH + GameHelper.GetCleanUnitName(unit.unitType) + "/" + ATTACK_PATH, 1, sounds.attackCount);

        unitView.SetState(UnitView.State.Attack);
        DestroyTiles();
        unitView.Flip(GameHelper.DoFlipUnit(mapPosition.x, victim.Position.x));
        if(GameHelper.IsUnitArcher(Unit.unitType))
        {
            StartCoroutine(InstantiateArrow(transform.position, 0.9f, victim.Position));
        }
        else
        {
            SoundManager.Instance.PlayRandomSoundFromResources(UNTIS_PATH + GameHelper.GetCleanUnitName(unit.unitType) + "/attack_", 1, 2);
        }
        StartCoroutine(OnAttackCompleted(1f));
    }

    private IEnumerator InstantiateArrow(Vector3 pos, float delay, MapVector position, bool special = false)
    {
        if(special && unit.unitType == UNIT_TYPE.CROSSBOWMAN)
        {
            SoundManager.Instance.PlaySoundFromResources(UNTIS_PATH + GameHelper.GetCleanUnitName(unit.unitType) + "/special");
        }
        yield return new WaitForSeconds(delay);
        if (special && sounds.hasSoundOnSpecial && unit.unitType != UNIT_TYPE.CROSSBOWMAN)
        {
            SoundManager.Instance.PlaySoundFromResources(UNTIS_PATH + GameHelper.GetCleanUnitName(unit.unitType) + "/special");
        }
        else if(GameHelper.IsUnitArcher(unit.unitType))
        {
            SoundManager.Instance.PlayRandomSoundFromResources(UNTIS_PATH + GameHelper.GetCleanUnitName(unit.unitType) + "/attack_", 1, 2);
        }
        Arrow arrow = ResourcesManager.InstantiateGameObject<Arrow>(ARROW_PATH);
        arrow.transform.position = pos;
        arrow.SetDestination(position);
    }

    private IEnumerator OnAttackCompleted(float delay)
    {
        yield return new WaitForSeconds(delay);
        unitView.SetState(UnitView.State.Start);
    }

    private void UpdateHealthBar()
    {
        float percent = (unit.CurrentHp / unit.hp) * 100f;
        healthBar.SetHealth(percent);
    }

    public IEnumerator Damage(float delay, Unit attacker, float damage, BaseController.OnActionCompleted onActionCompleted)
    {
        unitView.Flip(GameHelper.DoFlipUnit(mapPosition.x, attacker.Position.x));

        yield return new WaitForSeconds(delay);
        SoundManager.Instance.PlayRandomSoundFromResources(OTHER_PATH + HIT_PATH, 1, 5);

        Damage(damage);

        onActionCompleted(attacker, unit);
        onActionCompleted = null;
    }

    private void Damage(float damage)
    {
        unit.Damage(damage);
        AddText("-" + (int)damage, Color.red);
        UpdateHealthBar();
    }


    public void Die(Unit killer, BaseController.OnActionCompleted onActionCompleted)
    {

        if (GameHelper.IsUnitArcher(killer.unitType))
        {
            unitView.SetState(UnitView.State.Die_Arrow);
            if(unit.unitType == UNIT_TYPE.LORD)
            {
                SoundManager.Instance.PlaySoundFromResources(OTHER_PATH + "lorddie");
            }
            else
            {
                SoundManager.Instance.PlayRandomSoundFromResources(OTHER_PATH + DEATH_ARROW_PATH, 1, 5);
            }
        }
        else
        {
            unitView.SetState(UnitView.State.Die_Sword);
            if (unit.unitType == UNIT_TYPE.LORD)
            {
                SoundManager.Instance.PlaySoundFromResources(OTHER_PATH + "lorddie");
            }
            else
            {
                SoundManager.Instance.PlayRandomSoundFromResources(OTHER_PATH + DEATH_SWORD_PATH, 1, 5);
            }
        }
        map.SetTile(mapPosition, Tile.TileType.EMPTY);
        StartCoroutine(OnDieCompleted(killer, 1.5f, onActionCompleted));
    }

    private IEnumerator OnDieCompleted(Unit killer, float time, BaseController.OnActionCompleted onActionCompleted)
    {
        yield return new WaitForSeconds(time);
        onActionCompleted(killer);
        Destroy(gameObject);
    }

    public void UseSpecialPower(MapVector position)
    {
        SoundManager.Instance.PlaySoundFromResources(UNTIS_PATH + GameHelper.GetCleanUnitName(unit.unitType) + "/" + SPECIAL_PATH + "1");
        unitView.SetState(UnitView.State.Special);
        DestroyTiles();
        if (GameHelper.IsUnitArcher(Unit.unitType))
        {
            StartCoroutine(InstantiateArrow(transform.position, 0.9f, position, true));
        }
        else if(unit.unitType == UNIT_TYPE.LORD)
        {
            AttackAll();
        }
        else if(sounds.hasSoundOnSpecial)
        {
            SoundManager.Instance.PlaySoundFromResources(UNTIS_PATH + GameHelper.GetCleanUnitName(unit.unitType) + "/special");
        }
        StartCoroutine(OnSpecialPowerCompleted(1f));
    }

    private void AttackAll()
    {
        SoundManager.Instance.PlaySoundFromResources(UNTIS_PATH + GameHelper.GetCleanUnitName(unit.unitType) + "/special");
        List<Unit> enemies = Gameplay.Instance.GetAllEnemyUnits(player);
        float x = unitView.isFlipped ? -10f : 30f;
        Vector3 pos = new Vector3(x, transform.position.y, transform.position.z);
        foreach(Unit enemy in enemies)
        {
            StartCoroutine(InstantiateArrow(pos, 0f, enemy.Position));
        }
    }

    private IEnumerator OnSpecialPowerCompleted(float delay)
    {
        yield return new WaitForSeconds(1f);
        unitView.SetState(UnitView.State.Start);
    }

    private IEnumerator RemoveAllPositiveEffects(float delay)
    {
        yield return new WaitForSeconds(delay);
        unit.RemoveAllPositiveEffects();
    }

    public void AffectFromSpecialPower(Unit initiator, SPECIAL_POWER_TYPE type, BattleAction.UnitSpecialPowerData affectedUnit, BaseController.OnActionCompleted onActionCompleted)
    {
        float delay = 0f;
        switch (type)
        {
            case SPECIAL_POWER_TYPE.SHIELD:
                SetShield(affectedUnit.primaryParameter, affectedUnit.duration);
                Heal(affectedUnit.secondaryParameter);
                onActionCompleted(initiator);
                break;
            case SPECIAL_POWER_TYPE.AIMED_SHOT:
                delay = GameHelper.GetDamageDelay(initiator, unit.Position);
                StartCoroutine(Damage(delay, initiator, affectedUnit.primaryParameter, onActionCompleted));
                StartCoroutine(RemoveAllPositiveEffects(delay + 0.1f));
                break;
            case SPECIAL_POWER_TYPE.INCREASED_DAMAGE:
                SetIncreasedDamage(affectedUnit.primaryParameter, affectedUnit.duration);
                onActionCompleted(initiator);
                break;
            case SPECIAL_POWER_TYPE.ATTACK_ALL:
                StartCoroutine(Damage(0.1f, initiator, affectedUnit.primaryParameter, onActionCompleted));
                break;
            case SPECIAL_POWER_TYPE.HEAL_ALL:
                Heal(affectedUnit.primaryParameter);
                onActionCompleted(initiator);
                break;
            case SPECIAL_POWER_TYPE.POISON_SHOT:
                delay = GameHelper.GetDamageDelay(initiator, unit.Position);
                StartCoroutine(Damage(delay, initiator, affectedUnit.primaryParameter, onActionCompleted));
                StartCoroutine(SetPoison(delay + 0.1f, affectedUnit.secondaryParameter, affectedUnit.duration));
                break;
            case SPECIAL_POWER_TYPE.INVISIBILITY:
                StartCoroutine(SetInvisibility(0.5f, affectedUnit.duration));
                onActionCompleted(initiator);
                break;
            case SPECIAL_POWER_TYPE.BURNING:
                StartCoroutine(Damage(0.1f, initiator, affectedUnit.primaryParameter, onActionCompleted));
                SetBurning(affectedUnit.secondaryParameter, affectedUnit.duration);
                break;
            case SPECIAL_POWER_TYPE.RAGE:
                SetShield(affectedUnit.primaryParameter, affectedUnit.duration);
                SetIncreasedDamage(affectedUnit.secondaryParameter, affectedUnit.duration);
                onActionCompleted(initiator);
                break;
            case SPECIAL_POWER_TYPE.TAUNT:
                SetTaunt(affectedUnit.primaryParameter, affectedUnit.duration);
                onActionCompleted(initiator);
                break;
        }
    }

    private void AddText(string text, Color color)
    {
        InfoText info = ResourcesManager.InstantiateGameObject<InfoText>(TEXT_PATH, transform, new Vector3(0f, 0f, -1f));
        info.SetText(text, color);
    }

    private void AddEffect(SPECIAL_POWER_TYPE type)
    {
        effectsBar.AddEffect(type);
    }

    public void OnRemoveEffect(SPECIAL_POWER_TYPE type)
    {
        if(type == SPECIAL_POWER_TYPE.INVISIBILITY)
        {
            unitView.SetInvisible(false);
        }
        else if(type == SPECIAL_POWER_TYPE.BURNING)
        {
            unitView.RemoveFire();
        }
        effectsBar.RemoveEffect(type);
    }

    private void Heal(float percent)
    {
        float heal = unit.Heal(percent);
        UpdateHealthBar();
        AddText("+" + (int)heal, Color.green);
    }

    private void SetShield(float shield, int duration)
    {
        unit.SetShield(shield, duration);
        AddEffect(SPECIAL_POWER_TYPE.SHIELD);
    }

    private void SetIncreasedDamage(float increased, int duration)
    {
        unit.SetIncreasedDamage(increased, duration);
        AddEffect(SPECIAL_POWER_TYPE.INCREASED_DAMAGE);
    }

    private void SetTaunt(float taunt, int duration)
    {
        unit.SetTaunt(taunt, duration);
        AddEffect(SPECIAL_POWER_TYPE.TAUNT);
    }

    private IEnumerator SetPoison(float delay, float poison, int duration)
    {
        yield return new WaitForSeconds(delay);
        unit.SetPoison(poison, duration);
        AddEffect(SPECIAL_POWER_TYPE.POISON_SHOT);
    }

    private void SetBurning(float burning, int duration)
    {
        unit.SetBurning(burning, duration);
        unitView.AddFire();
        AddEffect(SPECIAL_POWER_TYPE.BURNING);
    }


    private IEnumerator SetInvisibility(float delay, int duration)
    {
        yield return new WaitForSeconds(delay);
        unitView.SetInvisible(true);
        unit.SetInvisibility(duration);
        AddEffect(SPECIAL_POWER_TYPE.INVISIBILITY);
    }


    private void CheckPoison()
    {
        if(unit.Poison > 0f)
        {
            float damage = unit.hp * unit.Poison;
            if (damage >= unit.CurrentHp)
                damage = unit.CurrentHp - 1f;
            Damage(damage);
        }
    }

    private void CheckFlip()
    {
        if ((int)transform.localPosition.x == Map.GRID_WIDTH - 1)
            unitView.Flip(false);
        else if ((int)transform.localPosition.x == 0)
            unitView.Flip(true);
   
    }

	private void Update() 
	{
	    if(startMoving)
        {
            float step = speed * Time.deltaTime;
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, destination, step);
            if(transform.localPosition.Equals(destination))
            {
                OnMoveCompleted();
            }
        }
	}
	
    private void RefreshPosition()
    {
        mapPosition = new MapVector((int)transform.localPosition.x, (int)transform.localPosition.y);
        unit.SetPosition(mapPosition);
    }

    public void OnTurnStarted()
    {
        SoundManager.Instance.PlayRandomSoundFromResources(UNTIS_PATH + GameHelper.GetCleanUnitName(unit.unitType) + "/" + START_PATH, 1, sounds.startCount);
        CreateTiles();
        CheckPoison();
    }

    public void OnTurnFinished()
    {
        DestroyTiles();
    }

    private void CreateTiles()
    {
        tiles = new List<UnitTile>();

        GameHelper.UnitRange moveRange = GameHelper.GetUnitRange(mapPosition.x, mapPosition.y, unit);

        for (int x = moveRange.minX; x < moveRange.maxX; x++)
        {
            for (int y = moveRange.minY; y < moveRange.maxY; y++)
            {
                UnitTile tile = new UnitTile(new Vector2(x, y), null);
                if (!tiles.Contains(tile))
                {
                    SpriteRenderer sr = ResourcesManager.InstantiateGameObject<SpriteRenderer>(TILE_PATH);
                    sr.transform.position = new Vector3(x * 2 + 1f, y * 2 + 1f, 0);
                    sr.transform.parent = tilesPlacement;
                    sr.color = GetTileColor(x, y);
                    tile.spriteRenderer = sr;
                    tiles.Add(tile);
                }
            }
        }

    }

    private Color GetTileColor(int x, int y)
    {
        Color color = GameHelper.GetColor(129, 83, 0);
        Tile tile = map.GetTile(x, y);

        if (x == mapPosition.x && y == mapPosition.y)
        {
            color = GameHelper.GetColor(6, 173, 56);
            chosen.SetActive(true);
        }
        else
        {
            switch (tile.CurrentTileType)
            {
                case Tile.TileType.EMPTY:
                    color = GameHelper.GetColor(234, 165, 60);
                    break;
                case Tile.TileType.UNIT:
                    UNIT_TEAM unitTeam = unit.UnitTeam;
                    UNIT_TEAM tileTeam = tile.Unit.UnitTeam;
                    if (unitTeam != tileTeam && map.CanUnitBeAttacked(unit, new MapVector(x, y)))
                        color = GameHelper.GetColor(163, 0, 0);
                    break;
            }
        }
        return color; 
    }

    private void DestroyTiles()
    {
        chosen.SetActive(false);
        for (int i = tiles.Count - 1; i >= 0; i--)
        {
            Destroy(tiles[i].spriteRenderer.gameObject);
        }
        tiles.Clear();
    }
}
