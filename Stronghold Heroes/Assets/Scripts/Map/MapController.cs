﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class MapController : MonoBehaviour 
{
    private const string LEVELS_PATH = "Data/Levels";

    [Serializable]
    private class LevelsData
    {
        public List<Player> players;

        public LevelsData()
        {
            players = new List<Player>();
        }
    }

    private bool isBriefing;

    public List<GameObject> levels = new List<GameObject>();

    public GameObject mainGUI;

    public BriefingGUIController briefingGUIController;

    public Transform avatar;

    private LevelsData levelsData;

    private static MapController instance;
    public static MapController Instance { get { return instance; } }

    private void Awake()
    {
        if (instance != null & instance != this)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        EventManager.onBackPressed += OnQuit;
        EventManager.onSceneLoad += EventManager_onSceneLoad;
    }

    private void Start ()
    {
        MusicManager.Instance.PlayOneClip("MainMenu");
        InitLevels();
        InitProgress();
	}

    private void OnDestroy()
    {
        EventManager.onBackPressed -= OnQuit;
        EventManager.onSceneLoad -= EventManager_onSceneLoad;
    }

    private void InitProgress()
    {
        List<PlayerProfile.Level> levelsData = PlayerProfile.GetProgress();

        for (int i = 0; i < levels.Count; i++)
        {
            if (levelsData[i].state == 0)
            {
                levels[i].SetActive(false);
            }
            else if (levelsData[i].state == 1)
            {
                levels[i].transform.GetChild(0).gameObject.SetActive(false);
                avatar.position = levels[i].transform.position;
            }
            else if (levelsData[i].state == 2 && i == levels.Count - 1)
            {
                avatar.position = levels[i].transform.position;
            }
        }
    }
	
    private void InitLevels()
    {
        levelsData = new LevelsData();
        TextAsset textAsset = ResourcesManager.Load<TextAsset>(LEVELS_PATH);
        JsonUtility.FromJsonOverwrite(textAsset.text, levelsData);
    }

    public void OpenBriefing(int level)
    {
        mainGUI.SetActive(false);
        briefingGUIController.gameObject.SetActive(true);
        Player enemy = new Player(levelsData.players[level - 1]);
        briefingGUIController.Init(level - 1, enemy);
        isBriefing = true;
    }

    public void OnQuit()
    {
        if (isBriefing)
        {
            mainGUI.SetActive(true);
            briefingGUIController.gameObject.SetActive(false);
            isBriefing = false;
        }
        else
        {
            Core.Instance.LoadScene("MainMenu");
        }
    }

    private void EventManager_onSceneLoad()
    {
        mainGUI.SetActive(false);
        briefingGUIController.gameObject.SetActive(false);
    }
}
