﻿using UnityEngine;
using UnityEngine.EventSystems;

public class LevelButton : MonoBehaviour 
{
    private string levelNumber;

    private bool isClick = false;

    private void Start()
    {
        levelNumber = gameObject.name;
    }

    private void OnMouseDown()
    {
        isClick = true; 
    }

    private void OnMouseUp()
    {
        if (isClick && !EventSystem.current.IsPointerOverGameObject())
        {
            Vector3 ray = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(ray, Vector2.zero);
            if (hit.transform != null)
            {
                if (hit.transform.gameObject == gameObject)
                {
                    int level = 0;
                    int.TryParse(levelNumber, out level);
                    MapController.Instance.OpenBriefing(level);
                }
            }
        }
        isClick = false;
    }

}
