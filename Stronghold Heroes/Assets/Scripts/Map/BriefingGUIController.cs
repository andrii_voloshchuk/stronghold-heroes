﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class BriefingGUIController : MonoBehaviour 
{
    private const string UNIT_BUTTON_PATH = "GUI/UnitButtonBriefing";

    [Serializable]
    public class UnitBattleButton
    {
        public Unit unit;
        public Button button;
        public Text level;

        public bool isEmpty;

        private OnClickPanel onClick;

        public void Init(OnClickPanel onClick)
        {
            this.onClick = onClick; 
            button.onClick.AddListener(delegate { OnClick(); });
        }

        private void OnClick()
        {
            if (onClick != null && unit != null)
            {
                SetEmpty();
                onClick(unit);
                unit = null;
            }
        }

        public void SetUnit(Unit unit)
        {
            isEmpty = false;
            button.image.enabled = true;
            this.unit = new Unit(unit);
            button.image.sprite = SpriteDictionary.Instance.GetUnitSprite(unit.unitType);
            level.text = unit.level.ToString();
        }

        public void SetEmpty()
        {
            isEmpty = true;
            button.image.enabled = false;
            level.text = string.Empty;
        }
    }

    public delegate void OnClick(Unit unit);
    public delegate void OnClickPanel(Unit unit);

    [Header("Enemy")]
    public List<UnitBattleButton> enemyUnits;

    public Image enemyAvatar;

    public Text enemyName;

    [Space(8)]
    [Header("Player")]
    public List<UnitBattleButton> playerUnits;

    public Image playerAvatar;

    public Text playerName;

    public RectTransform unitsPlacement;

    public Button buttonToBattle;

    private Player player;
    private Player enemy;

    private List<Unit> remainedUnits;

    private List<UnitButtonBriefing> unitButtons;

    private int level;

    public void Init(int level, Player enemy)
    {
        this.level = level;
        this.enemy = enemy;
        player = PlayerProfile.GetPlayer();

        for(int i = 0; i < enemy.units.Count; i++)
        {
            enemyUnits[i].SetUnit(enemy.units[i]);
        }

        enemyAvatar.sprite = SpriteDictionary.Instance.GetAvatarSprite(enemy.avatar);
        enemyName.text = TextDictionary.GetText(enemy.avatar.ToString());

        playerAvatar.sprite = SpriteDictionary.Instance.GetAvatarSprite(player.avatar);
        playerName.text = player.name;

        player.units.Sort();


        remainedUnits = new List<Unit>(player.units);
        unitButtons = new List<UnitButtonBriefing>();


        int size = 6;

        for (int i = 0; i < size; i++)
        {
            playerUnits[i].Init(OnClickPanelUnit);
            playerUnits[i].SetUnit(player.units[i]);
            remainedUnits.Remove(player.units[i]);
        }
        if(remainedUnits.Count > 0)
        {
            UpdateScrollList(remainedUnits);
        }
    }


    private void UpdateScrollList(List<Unit> units)
    {
        DestroyUnitButtons();

        units.Sort();

        float startPosX = 50f;
        float startPosY = -50f;

        float spacing = 100f;

        int elementsPerRow = 4;
        int index = 0;
        int row = 0;


        for (int i = 0; i < units.Count; i++)
        {
            if (index >= elementsPerRow)
            {
                index = 0;
                row++;
            }
            UnitButtonBriefing ub = ResourcesManager.InstantiateGameObject<UnitButtonBriefing>(UNIT_BUTTON_PATH);
            ub.transform.SetParent(unitsPlacement);

            RectTransform rt = ub.GetComponent<RectTransform>();
            float posX = startPosX + spacing * index;
            float posY = startPosY - spacing * row;
            rt.anchoredPosition = new Vector2(posX, posY);
            rt.localScale = Vector3.one;

            ub.SetUnit(units[i], OnClickUnit);
            unitButtons.Add(ub);
            index++;
        }

        unitsPlacement.sizeDelta = new Vector2(unitsPlacement.sizeDelta.x, spacing * (row + 1) + spacing);
        CheckButton();

    }


    private void CheckButton()
    {
        int count = playerUnits.FindAll(i => !i.isEmpty).Count;
        buttonToBattle.interactable = count > 0;
    }

    private void OnClickPanelUnit(Unit unit)
    {
        remainedUnits.Add(unit);
        UpdateScrollList(remainedUnits);
    }

    private void OnClickUnit(Unit unit)
    {
        for(int i = 0; i < playerUnits.Count; i++)
        {
            if(playerUnits[i].isEmpty)
            {
                playerUnits[i].SetUnit(unit);
                remainedUnits.Remove(unit);
                UpdateScrollList(remainedUnits);
                break;
            }
        }
    }

    private void DestroyUnitButtons()
    {
        for (int i = unitButtons.Count - 1; i >= 0; i--)
        {
            Destroy(unitButtons[i].gameObject);
        }
        unitButtons.Clear();
    }


    public void ToBattle()
    {
        Player temp = new Player(player.name);
        temp.avatar = player.avatar;
        temp.units = new List<Unit>();
        enemy.isAi = true;
        foreach(UnitBattleButton ubb in playerUnits)
        {
            if(!ubb.isEmpty)
            {
                temp.units.Add(ubb.unit);
            }
        }
        if (temp.units.Count > 0)
        {
            Core.Instance.StartBattle(GAME_MODE.CAMPAIGN, temp, enemy, level);
        }
    }
}
