﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class UnitButtonBriefing : MonoBehaviour
{
    public Text textName;
    public Text textLevel;

    private Button button;

    private Unit unit;

    private BriefingGUIController.OnClick onClick;

    private void Awake()
    {
        button = GetComponent<Button>();
    }

    public void SetUnit(Unit unit, BriefingGUIController.OnClick onClick)
    {
        this.onClick = onClick;
        this.unit = unit;
        Sprite sprite = SpriteDictionary.Instance.GetUnitSprite(unit.unitType);
        button.image.sprite = sprite;
        textName.text = GameHelper.GetCleanUnitName(unit.unitType);
        textLevel.text = "lvl " + unit.level;
    }

    public void OnClick()
    {
        if(onClick != null)
        {
            onClick(unit);
        }
    }
}
