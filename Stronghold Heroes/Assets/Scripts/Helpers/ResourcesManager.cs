﻿using UnityEngine;

public class ResourcesManager 
{

    public const string UNITS_FOLDER = "Gameplay/Units/";

	public static GameObject InstantiateGameObject(string path)
    {
        GameObject go = GameObject.Instantiate(Load(path) as GameObject);
        return go;
    }

    public static T InstantiateGameObject<T>(string path) where T : Component
    {
        return InstantiateGameObject(path).GetComponent<T>();
    }

    public static GameObject InstantiateGameObject(string path, Transform parent, Vector3 position)
    {
        GameObject go = InstantiateGameObject(path);
        go.transform.parent = parent;
        go.transform.localPosition = position;
        return go;
    }

    public static T InstantiateGameObject<T>(string path, Transform parent, Vector3 position) where T : Component
    {
        return InstantiateGameObject(path, parent, position).GetComponent<T>();
    }

    public static T Load<T>(string path)
    {
        return (T)(object)Load(path);
    }

    public static Object Load(string path)
    {
        return Resources.Load(path);
    }

    public static GameObject InstantiateGameObject(GameObject gameObject, Transform parent, Vector3 position)
    {
        GameObject go = GameObject.Instantiate(gameObject);
        go.transform.parent = parent;
        go.transform.localPosition = position;
        return go;
    }

    public static T InstantiateGameObject<T>(GameObject gameObject, Transform parent, Vector3 position) where T : Component
    {
        return InstantiateGameObject(gameObject, parent, position).GetComponent<T>();
    }
}
