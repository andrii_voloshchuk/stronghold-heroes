﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class SpriteDictionary : MonoBehaviour 
{
    [Serializable]
    public struct UnitSpriteReference
    {
        public UNIT_TYPE type;
        public Sprite sprite;
    }

    [Serializable]
    public struct SpecialPowerSpriteReference
    {
        public SPECIAL_POWER_SPRITE type;
        public Sprite sprite;
    }

    [Serializable]
    public struct AvatarSpriteReference
    {
        public AVATAR type;
        public Sprite sprite;
    }

    [Serializable]
    public struct UnitTeamSpriteReference
    {
        public UNIT_TEAM_SPRITE type;
        public Sprite sprite;
    }

    [Header("Units")]
    public UnitSpriteReference[] unitSprites;

    [Space(8)]
    [Header("Special Powers")]
    public SpecialPowerSpriteReference[] specialPowerSprites;

    [Space(8)]
    [Header("Avatars")]
    public AvatarSpriteReference[] avatarSprites;

    [Space(8)]
    [Header("Avatars")]
    public UnitTeamSpriteReference[] teamSprites;

    private Dictionary<Enum, Sprite> dictionary;

    private static SpriteDictionary instance;
    public static SpriteDictionary Instance { get { return instance; } }

    public void Init()
    {
        instance = this;
        dictionary = new Dictionary<Enum, Sprite>();

        for (int i = 0; i < unitSprites.Length; i++)
        {
            dictionary.Add(unitSprites[i].type, unitSprites[i].sprite);
        }

        for (int i = 0; i < specialPowerSprites.Length; i++)
        {
            dictionary.Add(specialPowerSprites[i].type, specialPowerSprites[i].sprite);
        }

        for (int i = 0; i < avatarSprites.Length; i++)
        {
            dictionary.Add(avatarSprites[i].type, avatarSprites[i].sprite);
        }

        for (int i = 0; i < teamSprites.Length; i++)
        {
            dictionary.Add(teamSprites[i].type, teamSprites[i].sprite);
        }
    }

    public Sprite GetSprite(Enum type)
    {
        if (dictionary.ContainsKey(type))
        {
            return dictionary[type];
        }
        return null;
    }

    public Sprite GetUnitSprite(UNIT_TYPE type)
    {
        return GetSprite(type);
    }

    public Sprite GetSpecialPowerSprite(SPECIAL_POWER_SPRITE type)
    {
        return GetSprite(type);
    }

    public Sprite GetAvatarSprite(AVATAR type)
    {
        return GetSprite(type);
    }

    public Sprite GetTeamSprite(UNIT_TEAM_SPRITE type)
    {
        return GetSprite(type);
    }
}
