﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class RandomAnimation : MonoBehaviour 
{
    public string triggerName = "Start";

    private Animator animator;

    private float randomTimeDelta;
    private float randomTime;


	private void Start() 
	{
        animator = GetComponent<Animator>();
        randomTime = Random.Range(10f, 30f);
	}
	
	private void Update() 
	{
        randomTimeDelta += Time.deltaTime;
        if(randomTimeDelta >= randomTime)
        {
            randomTimeDelta = 0f;
            randomTime = Random.Range(10f, 20f);
            animator.SetTrigger(triggerName);
        }
    }
	
}
