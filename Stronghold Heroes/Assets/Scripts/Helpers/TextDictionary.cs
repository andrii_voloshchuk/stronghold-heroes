﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;

public class TextDictionary 
{
    private const string PATH = "Data/Dictionary";

    private static TextDictionary instance;

    private Dictionary<string, string> dictionary;

    public static void CreateInstance()
    {
        instance = new TextDictionary();
        LoadDictionary();
    }

    private static void LoadDictionary()
    {
        instance.dictionary = new Dictionary<string, string>();
        TextAsset textAsset = ResourcesManager.Load<TextAsset>(PATH);
        var node = JSON.Parse(textAsset.text);
        JSONClass jc = (JSONClass)node;

        for (int i = 0; i < jc.Count; i++)
        {
            instance.dictionary.Add(jc.GetKey(i), jc[i]);
        }
    }

    public static string GetText(string key)
    {
        if(instance.dictionary.ContainsKey(key))
        {
            return instance.dictionary[key];
        }
        return "KeyNotFound: " + key;
    }    

	public static string GetText(string key, params string[] parameters)
    {
        string text = GetText(key);
        return string.Format(text, parameters);
    }
}
