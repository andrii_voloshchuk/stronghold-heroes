﻿using UnityEngine;

public class AnimationSound : MonoBehaviour 
{
    public string soundName;

    public bool run;

    private bool stop;

	private void Update()
    {
        if(run)
        {
            if (!stop)
            {
                stop = true;
                SoundManager.Instance.PlayOneSound(soundName);
            }
            run = false;
        }
    }

    private void OnDisable()
    {
        stop = false;
        run = false;
    }
}
