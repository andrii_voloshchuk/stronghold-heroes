﻿using System;
using System.Collections.Generic;

[Serializable]
public class UnitsInitialData
{
    public List<Unit> units;

    public UnitsInitialData()
    {
        units = new List<Unit>();
    }
}
