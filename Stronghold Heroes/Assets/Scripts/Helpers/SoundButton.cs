﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class SoundButton : MonoBehaviour
{
    public string soundName = "click";

	private void Awake()
	{
        if (!string.IsNullOrEmpty(soundName))
        {
            GetComponent<Button>().onClick.AddListener(delegate { SoundManager.Instance.PlayOneSound(soundName); });
        }
	}
}
