﻿using System;

[Serializable]
public class MapVector 
{
    public int x;
    public int y;

    public MapVector(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return false;
        if (obj.GetType() != GetType())
            return false;
        MapVector other = (MapVector)obj;
        return x == other.x && y == other.y;
    }

    public override int GetHashCode()
    {
        return x * x + (x * y);
    }

    public override string ToString()
    {
        return "(" + x + ", " + y + ")"; 
    }
	
}
