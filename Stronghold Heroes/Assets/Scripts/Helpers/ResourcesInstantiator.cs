﻿using System.IO;
using UnityEngine;

public class ResourcesInstantiator : MonoBehaviour 
{
    public enum InstantiateMethod
    {
        Awake,
        Start,
        OnLoad,
        Manual
    }

    public InstantiateMethod instantiateMethod;

    public string folderPath;
    public string[] prefabs;

    public bool loadAsChildren;

	private void Awake()
	{
        if (instantiateMethod.Equals(InstantiateMethod.Awake))
        {
            InstantiatePrefab();
        }
	}

	private void Start() 
	{
        if (instantiateMethod.Equals(InstantiateMethod.Start))
        {
            InstantiatePrefab();
        }
	}

    private void OnLevelWasLoaded(int level)
    {
        if (instantiateMethod.Equals(InstantiateMethod.OnLoad))
        {
            InstantiatePrefab();
        }
    }

    public void ManualInstantiate(string path = null, string[] prefabs = null)
    {
        if (instantiateMethod.Equals(InstantiateMethod.Manual))
        {
            if(!string.IsNullOrEmpty(path))
            {
                folderPath = path;
            }
            if (prefabs != null && prefabs.Length > 0)
            {
                this.prefabs = prefabs;
            }
            InstantiatePrefab();
        }
    }
	
	private void InstantiatePrefab() 
	{
	     if (!string.IsNullOrEmpty(folderPath))
        {
            foreach (string prefab in prefabs)
            {
                if (string.IsNullOrEmpty(prefab))
                    continue;

                string path = Path.Combine(folderPath, prefab);

                GameObject resource = Resources.Load(path) as GameObject;

                if (resource != null)
                {
                    GameObject go = null;

                    if (!loadAsChildren)
                    {
                        go = Instantiate(resource, resource.transform.position, resource.transform.rotation) as GameObject;

                    }
                    else
                    {
                        go = Instantiate(resource) as GameObject;
                        go.transform.parent = transform;
                        go.transform.localPosition = resource.transform.position;
                        go.transform.localRotation = resource.transform.rotation;
                        go.transform.localScale = resource.transform.localScale;
                    }

                    go.name = go.name.Replace("(Clone)", string.Empty);
                }
                else
                {
                    Debug.LogError("Cannot load resource on " + gameObject.name + " Game Object on path " + path);
                }
            }
        }
        else
        {
            Debug.LogError("Cannot load resources on " + gameObject.name + " Game Object. Path is empty");
        }

        if (!loadAsChildren)
            Destroy(gameObject);
	}

}
