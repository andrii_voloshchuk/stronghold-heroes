﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Animator))]
public class InteractableElement : MonoBehaviour 
{

    public string triggerName = "Start";
    public string onClickTriggerName = "Die";
    public string resetTriggerName = "Reset";

    public Vector3 destination;

    public float speed;

    private Vector3 startPosition;

    private Animator animator;

    private float randomTimeDelta;
    private float randomTime;

    private bool isMoving;

    private void Start()
    {
        animator = GetComponent<Animator>();
        randomTime = Random.Range(10f, 30f);
        startPosition = transform.localPosition;
    }

    private void OnMouseDown()
    {
        isMoving = false;
        animator.SetTrigger(onClickTriggerName);
        StartCoroutine(Reset(1f));
    }

    private IEnumerator Reset(float delay)
    {
        yield return new WaitForSeconds(delay);
        isMoving = false;
        randomTimeDelta = 0f;
        randomTime = Random.Range(10f, 30f);
        transform.localPosition = startPosition;
        animator.SetTrigger(resetTriggerName);
    }

    private void Update()
    {
        if (!isMoving)
        {
            randomTimeDelta += Time.deltaTime;
            if (randomTimeDelta >= randomTime)
            {
                randomTimeDelta = 0f;
                randomTime = Random.Range(10f, 20f);
                animator.SetTrigger(triggerName);
                isMoving = true;
            }
        }
        else
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, destination, Time.deltaTime * speed);
            if(transform.localPosition == destination)
            {
                StartCoroutine(Reset(0f));
            }
        }
    }

}
