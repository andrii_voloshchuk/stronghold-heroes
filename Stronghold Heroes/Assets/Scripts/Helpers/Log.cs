﻿using UnityEngine;

public class Log  
{
    private enum Color
    {
        black,
        blue,
        brown,	
        cyan,
        darkblue,
        green,
        grey,
        lightblue,
        lime,
        magenta,
        maroon,
        navy,
        olive,      
        orange,
        purple,	
        red,	
        silver,	
        teal,	
        white,	
        yellow
    }

    private static string WrapWithColor(string message, Color color)
    {
        return "<color=" + color.ToString() + ">" + message + "</color>";
    }

    public static void Battle(object message)
    {
        PrintLog(WrapWithColor("BATTLE: " + message.ToString(), Color.green));
    }

	public static void AI(object message)
    {
        PrintLog(WrapWithColor("AI: " + message.ToString(), Color.blue));
    }

    private static void PrintLog(string message)
    {
#if SHOW_LOG
        Debug.Log(message);
#endif
    }
}
