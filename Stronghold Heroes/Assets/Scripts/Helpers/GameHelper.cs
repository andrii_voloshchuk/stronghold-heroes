﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameHelper
{
    private const string UNITS_DATA_PATH = "Data/UnitsInitialData";

    private static UnitsInitialData uid;

    public struct UnitRange
    {
        public int minX;
        public int maxX;
        public int minY;
        public int maxY;

        public UnitRange(int minX, int maxX, int minY, int maxY)
        {
            this.minX = minX;
            this.maxX = maxX;
            this.minY = minY;
            this.maxY = maxY;
        }

        public override string ToString()
        {
            return "(" + minX + "," + minY + "),(" + maxX + "," + maxY + ")";
        }
    }

    public static float GetUnitPositionZ(float y)
    {
        return y - Map.GRID_HEIGTH;
    }

    public static bool DoFlipUnit(float x1, float x2)
    {
        return x1 < x2;
    }

    public static MapVector GetPositionOnGrid(Vector3 hit)
    {
        return new MapVector((int)(hit.x / 2), (int)(hit.y / 2));
    }

    public static string GetCleanUnitName(UNIT_TYPE unitType)
    {
        string unit = unitType.ToString();
        return unit[0] + unit.Substring(1).ToLower();
    }

    public static UnitRange GetUnitRange(int x, int y, Unit unit)
    {
        int minX = Mathf.Max(x + 1 - unit.range, 0);
        int maxX = Mathf.Min(x + unit.range, Map.GRID_WIDTH);
        int minY = Mathf.Max(y + 1 - unit.range, 0);
        int maxY = Mathf.Min(y + unit.range, Map.GRID_HEIGTH);
        return new UnitRange(minX, maxX, minY, maxY);
    }

    public static bool IsInsideUnitRange(MapVector position, MapVector unitPosition, Unit unit)
    {
        UnitRange unitRange = GetUnitRange(unitPosition.x, unitPosition.y, unit);
        return IsInsideRange(position, unitRange);
    }

    public static bool IsInsideRange(MapVector position, UnitRange unitRange)
    {
        return position.x >= unitRange.minX && position.x < unitRange.maxX && position.y >= unitRange.minY && position.y < unitRange.maxY;
    }

    public static T ToEnum<T>(Enum value)
    {
        return (T)Enum.Parse(typeof(T), value.ToString());
    }

    public static T ToEnum<T>(string value)
    {
        return (T)Enum.Parse(typeof(T), value);
    }

    public static float GetRandomFloat(float min, float max)
    {
        return UnityEngine.Random.Range(min, max);
    }

    public static int GetRandomInt(int min, int max, bool maxIncluded = false)
    {
        return UnityEngine.Random.Range(min, max + (maxIncluded ? 1 : 0));
    }

    public static float GetRandomUnitDamage(float damage)
    {
        float min = Mathf.Max(damage - damage / 4f, 0f);
        float max = Mathf.Max(damage + damage / 4f, 0f);
        return GetRandomFloat(min, max);
    }

    public static float GetRandomUnitSpecialPowerEffect(float effect)
    {
        float min = Mathf.Max(effect - effect / 8f, 0f);
        float max = Mathf.Max(effect + effect / 8f, 0f);
        return GetRandomFloat(min, max);
    }

    public static Color GetColor(int r, int g, int b)
    {
        return new Color(r / 255f, g / 255f, b / 255f);
    }

    public static string GetCleanSpecialPowerName(SPECIAL_POWER_TYPE type)
    {
        string name = type.ToString();
        name = name.Replace("_", " ");
        return name[0] + name.Substring(1).ToLower();
    }

    public static void LoadInitialData()
    {
        string json = ResourcesManager.Load<TextAsset>(UNITS_DATA_PATH).text;
        uid = JsonUtility.FromJson<UnitsInitialData>(json);
    }

    public static UnitsInitialData GetInitialData()
    {
        return uid;
    }

    public static List<Unit> GetRandomUnits(int count, int index = -1)
    {
        List<Unit> units = new List<Unit>();
        while (units.Count < count)
        {
            int i = index == -1 ? GetRandomInt(0, uid.units.Count) : index;
            Unit other = uid.units[i];
            Unit unit = new Unit(other);
            units.Add(unit);
        }
        return units;
    }

    public static bool IsUnitArcher(UNIT_TYPE type)
    {
        return type == UNIT_TYPE.ARCHER || type == UNIT_TYPE.CROSSBOWMAN;
    }

    public static Vector2 ToVector2(MapVector vector)
    {
        return new Vector2(vector.x, vector.y);
    }

    public static float GetDamageDelay(Unit attacker, MapVector victim)
    {
        float distance = Vector2.Distance(ToVector2(attacker.Position), ToVector2(victim));
        return distance * 0.05f + (IsUnitArcher(attacker.unitType) ? 0.9f : 0.5f);
    }

    public static bool IsInsideRange(Unit attacker, Unit victim, int range)
    {
        int x = attacker.Position.x;
        int y = attacker.Position.y;
        int minX = Mathf.Max(x + 1 - range, 0);
        int maxX = Mathf.Min(x + range, Map.GRID_WIDTH);
        int minY = Mathf.Max(y + 1 - range, 0);
        int maxY = Mathf.Min(y + range, Map.GRID_HEIGTH);
        UnitRange unitRange = new UnitRange(minX, maxX, minY, maxY);
        return IsInsideRange(victim.Position, unitRange);
    }

    public static float GetStatPlus(STAT_NAME stat, int level, bool isInPercent = false)
    {
        switch (stat)
        {
            case STAT_NAME.HEALTH:
                return 10f;
            case STAT_NAME.DAMAGE:
                return 5f;
            case STAT_NAME.RANGE:
                return level % 25 == 0 ? 1 : 0;
            case STAT_NAME.COOLDOWN:
                return level % 30 == 0 ? 1 : 0;
            case STAT_NAME.PRIMARY:
            case STAT_NAME.SECONDARY:
                if (!isInPercent)
                {
                    return (int)(level / 5f);
                }
                else
                {
                    return level % 5 == 0 ? 0.01f : 0f;
                }
        }
        return level;
    }


    public static Unit UpgradeUnit(Unit unit)
    {
        Unit data = uid.units.Find(i => i.unitType == unit.unitType);

        unit.hp += GetStatPlus(STAT_NAME.HEALTH, unit.level);
        unit.damage += GetStatPlus(STAT_NAME.DAMAGE, unit.level);
        if (unit.range < Map.GRID_WIDTH)
        {
            unit.range += (int)GetStatPlus(STAT_NAME.RANGE, unit.level);
        }
        if (unit.specialPower.coolDown > 1)
        {
            unit.specialPower.coolDown -= (int)GetStatPlus(STAT_NAME.COOLDOWN, unit.level);
        }
        if (data.specialPower.primaryParameter > 0)
        {
            bool isInPercent = data.specialPower.primaryParameter < 1f;
            unit.specialPower.primaryParameter += GetStatPlus(STAT_NAME.PRIMARY, unit.level, isInPercent);
        }
        if (data.specialPower.secondaryParameter > 0)
        {
            bool isInPercent = data.specialPower.secondaryParameter < 1f;
            unit.specialPower.secondaryParameter += GetStatPlus(STAT_NAME.SECONDARY, unit.level, isInPercent);
        }
        unit.level++;

        return unit;
    }

    public static Unit UpgradeUnit(Unit unit, int level)
    {
        Unit data = new Unit(uid.units.Find(i => i.unitType == unit.unitType));
        
        for (int i = data.level; i < level; i++)
        {
            data = new Unit(UpgradeUnit(data));
        }
        return data;
    }
}
